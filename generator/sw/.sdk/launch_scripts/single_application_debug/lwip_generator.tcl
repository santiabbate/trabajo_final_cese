connect -url tcp:127.0.0.1:3121
targets -set -nocase -filter {name =~"APU*"}
rst -system
after 3000
targets -set -filter {jtag_cable_name =~ "Digilent Arty Z7 003017A4C943A" && level==0} -index 1
fpga -file /mnt/Archivos/cese/trabajo_final/generator/sw/lwip_generator/_ide/bitstream/generator_wrapper.bit
targets -set -nocase -filter {name =~"APU*"}
loadhw -hw /mnt/Archivos/cese/trabajo_final/generator/sw/generator_wrapper/export/generator_wrapper/hw/generator_wrapper.xsa -mem-ranges [list {0x40000000 0xbfffffff}]
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*"}
source /mnt/Archivos/cese/trabajo_final/generator/sw/lwip_generator/_ide/psinit/ps7_init.tcl
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "*A9*#0"}
dow /mnt/Archivos/cese/trabajo_final/generator/sw/lwip_generator/Debug/lwip_generator.elf
configparams force-mem-access 0
targets -set -nocase -filter {name =~ "*A9*#0"}
con
