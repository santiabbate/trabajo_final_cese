// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "mm2s_dds_modulator,Vivado 2019.2" *)
module generator_mm2s_dds_modulator_0_0(S_AXI_CLK, S_AXI_ARESETN, S_AXI_AWREADY, 
  S_AXI_AWADDR, S_AXI_AWVALID, S_AXI_WREADY, S_AXI_WDATA, S_AXI_WSTRB, S_AXI_WVALID, 
  S_AXI_BREADY, S_AXI_BRESP, S_AXI_BVALID, S_AXI_ARREADY, S_AXI_ARADDR, S_AXI_ARVALID, 
  S_AXI_RREADY, S_AXI_RDATA, S_AXI_RRESP, S_AXI_RVALID, dds_en_o, m_axis_modulation_tdata, 
  m_axis_modulation_tvalid, m_axis_modulation_tready);
  input S_AXI_CLK;
  input S_AXI_ARESETN;
  output S_AXI_AWREADY;
  input [31:0]S_AXI_AWADDR;
  input S_AXI_AWVALID;
  output S_AXI_WREADY;
  input [31:0]S_AXI_WDATA;
  input [3:0]S_AXI_WSTRB;
  input S_AXI_WVALID;
  input S_AXI_BREADY;
  output [1:0]S_AXI_BRESP;
  output S_AXI_BVALID;
  output S_AXI_ARREADY;
  input [31:0]S_AXI_ARADDR;
  input S_AXI_ARVALID;
  input S_AXI_RREADY;
  output [31:0]S_AXI_RDATA;
  output [1:0]S_AXI_RRESP;
  output S_AXI_RVALID;
  output dds_en_o;
  output [71:0]m_axis_modulation_tdata;
  output m_axis_modulation_tvalid;
  input m_axis_modulation_tready;
endmodule
