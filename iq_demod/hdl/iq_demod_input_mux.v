`timescale 1ns / 1ps
/**
 * @file iq_demod_input_mux.v
 * @author Santiago Abbate
 * @brief CESE - Trabajo Final - Control de etapa digital de RADAR pulsado multipropósito.
 * Multiplexer for iq demodulator input selection.
 */

module iq_demod_input_mux #(parameter DATA_BITS = 14)(
    /* Input from internal waveform generator */
    input wire   [31:0] s_axis_internal_tdata,
    input wire          s_axis_internal_tvalid,
    /* Input from external signal */
    input wire   [15:0] s_axis_external_tdata,
    input wire          s_axis_external_tvalid,
    output wire         s_axis_external_tready,
    /* Selection input */
    input wire          sel_i,
    /* Data output */
    output wire   [31:0] m_axis_out_tdata,
    output wire          m_axis_out_tvalid,
    input wire           m_axis_out_tready
);
    
    reg [31:0] out_tdata;
    
    always @(*)
    begin
        case (sel_i)
            1'b0: out_tdata = {{18{s_axis_external_tdata[DATA_BITS-1]}},s_axis_external_tdata[DATA_BITS-1:0]};
            1'b1: out_tdata = {{18{s_axis_internal_tdata[16 + DATA_BITS-1]}},s_axis_internal_tdata[16 + DATA_BITS-1:16]};
//            default: 
        endcase
    end
    
    assign m_axis_out_tvalid = sel_i ? s_axis_internal_tvalid : s_axis_external_tvalid;
    assign m_axis_out_tdata = out_tdata;

    assign s_axis_external_tready = m_axis_out_tready;

endmodule