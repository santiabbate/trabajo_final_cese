`timescale 1ns / 1ps
/**
 * @file axis_demod_packetizer.sv
 * @author Santiago Abbate
 * @brief CESE - Trabajo Final - Control de etapa digital de RADAR pulsado multipropósito.
 * Pass-through AXI-Stream Data with tlast generation after N configured samples.
 */

module axis_demod_packetizer(
    input wire aclk,
    input wire aclken,
    input wire aresetn,

    /* AXI-Stream input signals */
    input wire [63:0]   s_axis_input_tdata,
    input wire          s_axis_input_tvalid,
    output wire          s_axis_input_tready,

    /* AXI-Stream output signals */
    output wire [63:0]   m_axis_output_tdata,
    output wire          m_axis_output_tvalid,
    input wire          m_axis_output_tready,
    output wire         m_axis_output_tlast,

    /* Packet length configuration */
    input wire [31:0] packet_length_i
);

    // AXI-Stream TLAST Circuit

    /* Signal to count the amount of debug samples thrown to the bus */
    
    reg [31:0] packet_counter;

    reg [63:0] tdata_reg;
    reg tvalid_reg;

    always @(posedge aclk)
    begin
        if (aresetn == 0) begin
            packet_counter <= 0;
        end
        else
        begin

            tdata_reg <= s_axis_input_tdata;
            tvalid_reg <= s_axis_input_tvalid;

            if (aclken)
            begin
                if(m_axis_output_tready && (s_axis_input_tvalid || m_axis_output_tlast))
                begin 
                    if (packet_counter == packet_length_i - 1)
                    begin
                        packet_counter <= 0;
                    end
                    else 
                    begin
                        packet_counter <= packet_counter + 1;
                    end
                end
            end
            else
            begin
                packet_counter <= 0;
            end
        end
    end

    /* Output signals */
    assign m_axis_output_tdata = tdata_reg; // Output data is the same as input data, but delayed one cycle
    assign m_axis_output_tvalid = tvalid_reg;
    assign s_axis_input_tready = 1'b1;  // Core is always ready to accept data

    assign m_axis_output_tlast = (packet_counter == packet_length_i - 1) ? 1 : 0;

endmodule