`timescale 1ns / 1ps
/**
 * @file mm2iq_config.v
 * @author Santiago Abbate
 * @brief CESE - Trabajo Final - Control de etapa digital de RADAR pulsado multipropósito.
 * AXI4-Lite Memory Mapped configuration IQ demodulator.
 * Top Wrapper for modulator and register instances.
 */

module mm2iq_config(
    /* AXI4-Lite Clock and reset signals */
    input wire          S_AXI_CLK,
    input wire          S_AXI_ARESETN,

    /* AXI4-lite slave signals */
    /* Write address signals */
    output wire         S_AXI_AWREADY,
    input wire   [31:0] S_AXI_AWADDR,
    input wire          S_AXI_AWVALID,
    /* Write data signals */
    output wire         S_AXI_WREADY,
    input wire   [31:0] S_AXI_WDATA,
    input wire   [3:0]  S_AXI_WSTRB,
    input wire          S_AXI_WVALID,
    /* Write response signals */
    input wire          S_AXI_BREADY,
    output wire  [1:0]  S_AXI_BRESP,
    output wire         S_AXI_BVALID,
    /* Read address signals */
    output wire         S_AXI_ARREADY,
    input wire   [31:0] S_AXI_ARADDR,
    input wire          S_AXI_ARVALID,
    /* Read data signals */	
    input wire          S_AXI_RREADY,
    output wire  [31:0] S_AXI_RDATA,
    output wire  [1:0]  S_AXI_RRESP,
    output wire         S_AXI_RVALID, 

    /* CIC Compiler configuration signals */
    output wire [31:0] S_AXIS_CIC_RATE_tdata,
    output wire S_AXIS_CIC_RATE_tready,
    output wire S_AXIS_CIC_RATE_tvalid,

    /* Local oscillator DDS configuration signals */
    output wire [31:0] S_AXIS_PHASE_LO_tdata,
    output wire S_AXIS_PHASE_LO_tready,
    output wire S_AXIS_PHASE_LO_tvalid,

    output wire [31:0] num_samples,
    output wire iq_en_o,

    output wire transfer_en,
    input wire transfer_tlast,

    output wire source_o
);

    wire [31:0] config_reg_0;
    wire [31:0] config_reg_1;
    wire [31:0] config_reg_2;
    wire [31:0] config_reg_3;

    /* Configuration Register 0 signals */
    /* Demodulator global enable */
    wire en = config_reg_0[0];
    /* Data transfer enable */
    assign transfer_en = config_reg_0[1]; // TODO: Parametrize transfer bit
    /* Signal source selection */
    assign source_o = config_reg_0[2];    // TODO: Parametrize source bit

    /* Map register outputs to corresponding AXI-Stream buses */
    assign S_AXIS_PHASE_LO_tdata = config_reg_1;
    assign S_AXIS_PHASE_LO_tvalid = en;

    assign S_AXIS_CIC_RATE_tdata = config_reg_2;

    assign num_samples = config_reg_3;
    
    /* Must assert S_AXIS_CIC_RATE_tvalid for one cycle only for new rate configuration */
    reg [31:0] cic_rate;
    reg S_AXIS_CIC_RATE_tvalid_reg;

    assign S_AXIS_CIC_RATE_tvalid = S_AXIS_CIC_RATE_tvalid_reg;

    always @(posedge S_AXI_CLK)
    begin
        if (!S_AXI_ARESETN)
        begin
            cic_rate <= 0;
        end
        else if(en)
        begin
            cic_rate <= config_reg_2;
        end
    end

    always @(*) begin
        S_AXIS_CIC_RATE_tvalid_reg = 0;
        if (config_reg_2 != cic_rate) begin
            S_AXIS_CIC_RATE_tvalid_reg = 1;
        end
        else begin
            S_AXIS_CIC_RATE_tvalid_reg = 0;
        end
    end

    assign iq_en_o = en;

    /* Registers */
    axi_lite_mm2iq_demod_registers registers(
    /* Register outputs */ 
    .config_reg_0_o(config_reg_0),
    .config_reg_1_o(config_reg_1),
    .config_reg_2_o(config_reg_2),
    .config_reg_3_o(config_reg_3),
    .S_AXI_CLK(S_AXI_CLK),
    .S_AXI_ARESETN(S_AXI_ARESETN),
    .S_AXI_AWREADY(S_AXI_AWREADY),
    .S_AXI_AWADDR(S_AXI_AWADDR),
    .S_AXI_AWVALID(S_AXI_AWVALID),
    .S_AXI_WREADY(S_AXI_WREADY),
    .S_AXI_WDATA(S_AXI_WDATA),
    .S_AXI_WSTRB(S_AXI_WSTRB),
    .S_AXI_WVALID(S_AXI_WVALID),
    .S_AXI_BREADY(S_AXI_BREADY),
    .S_AXI_BRESP(S_AXI_BRESP),
    .S_AXI_BVALID(S_AXI_BVALID),
    .S_AXI_ARREADY(S_AXI_ARREADY),
    .S_AXI_ARADDR(S_AXI_ARADDR),
    .S_AXI_ARVALID(S_AXI_ARVALID),
    .S_AXI_RREADY(S_AXI_RREADY),
    .S_AXI_RDATA(S_AXI_RDATA),
    .S_AXI_RRESP(S_AXI_RRESP),
    .S_AXI_RVALID(S_AXI_RVALID), 
    /* Transfer tlast signal */
    .transfer_tlast(transfer_tlast)
);

endmodule