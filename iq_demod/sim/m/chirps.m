% Script for simulated chirps generation %

N = 14; % Bus width for amplitude scaling

ts = 8e-9;  
fs = 1/ts;  % fs = 125 MHz
T = 100e-6; % Sweep length

t = 0:ts:T;

% Signal configuration
bw = 1e6;   % Bandwith
lo = 40e6;  % Local oscillator
R = 70;     % Decimator rate    

% Generate chirp centered in local oscillator
y = (2^(N-1)-1) .* chirp(t,lo - (bw/2),T, lo + (bw/2));

z = zeros(1,16);

% Build output vector from some signal sweeps and zeros
out = [y y y y z z z z z];

% Quantize to int
outint = int32(out);

% plot(abs(fftshift(fft(int32(x(1:12533))))))
% plot(abs(fftshift(fft(outint))))

% Save signal to file
fid = fopen('chirps.txt','w');
fprintf(fid,'%d\n',outint);
fclose(fid);

% Save parameters to file
fp = fopen('params.txt','w');
fprintf(fp,'%d,%d',lo/1e6,R);
fclose(fp);