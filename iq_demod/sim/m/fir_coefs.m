%%%%%% CIC filter parameters %%%%%%
R = 4; %% Decimation factor
M = 1; %% Differential delay
N = 3; %% Number of stages
B = 16; %% Coeffi. Bit-width
Fs = fs; %% (High) Sampling freq in Hz before decimation
Fc = fs/R/2; %% Pass band edge in Hz
%%%%%%% fir2.m parameters %%%%%%
L = 48; %% Filter order; must be even
Fo = 0.5;

%%%%%%% CIC Compensator Design using fir2.m %%%%%%
p = 2e3; %% Granularity
s = 0.25/p; %% Step sizeclos
f_p = [0:s:Fo]; %% Pass band frequency samples
f_s = (Fo+s):s:0.5; %% Stop band frequency samples
f_ = [f_p f_s]*2; %% Normalized frequency samples; 0<=f<=1
Mp = ones(1,length(f_p)); %% Pass band response; Mp(1)=1
Mp(2:end) = abs( M*R*sin(pi*f_p(2:end)/R)./sin(pi*M*f_p(2:end))).^N;
Mf = [Mp zeros(1,length(f_s))];
f_(end) = 1;
h = fir2(L,f_,Mf); %% Filter length L+1
h = h/max(h); %% Floating point coefficients
hz = round(h*power(2,B-1)-1); %% Fixed point coefficients
csvwrite('coefs',int32(hz))

