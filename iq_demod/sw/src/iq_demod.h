#ifndef __IQ_DEMOD
#define __IQ_DEMOD

#include <xil_io.h>
#include "xaxidma.h"

#define OS_FREERTOS 1

#define ERROR -1
#define SUCCESS 0

#define FCLK_MHZ    125U
#define FCLK_KHZ    (FCLK_MHZ * 1000)
#define FCLK        (FCLK_KHZ * 1000)
#define LO_PINC_BITS    30
#define LO_PINC_MASK ((1U << (LO_PINC_BITS)) - 1)

/* Reg 0 defines */
#define ENABLE_BIT 0
#define TRANSFER_EN_BIT 1
#define SOURCE_BIT 2

#define REG_0_OFFSET 0x0
#define REG_1_OFFSET 0x4
#define REG_2_OFFSET 0x8
#define REG_3_OFFSET 0xc
#define REG_4_OFFSET 0x10
#define REG_5_OFFSET 0x14

/* DMA transfer defines */
#define MAX_TRANSFER_SAMPLES 125000
#define MAX_TRANSFER_BYTES MAX_TRANSFER_SAMPLES * sizeof(u64)

typedef enum{
    LOCAL_OSC_10M = 10000,
    LOCAL_OSC_20M = 20000,
    LOCAL_OSC_30M = 30000,
    LOCAL_OSC_40M = 40000
}lo_freq_e;

typedef enum{
    RATE_4 = 4,
    RATE_5 = 5,
    RATE_8 = 8,
    RATE_16 = 16,
    RATE_40 = 40,
    RATE_70 = 70
}rates_e;

typedef enum{
    EXTERNAL = 0,
    INTERNAL = 1
}source_e;
typedef struct IQ_Demod
{
    /* General attributes */
    uint32_t address;
    uint8_t enabled;
    source_e source;
    /* Amount of samples to transfer from FPGA */
    uint32_t num_samples;
    /* Local oscillator */
    lo_freq_e lo_freq_khz;
    rates_e decimation_rate;
    /* AXI-DMA IP Core attributes */
    u32 axi_dma_device_id;
    XAxiDma axi_dma_inst;
    XAxiDma_Config *axi_dma_cfg_ptr;
    /* Data reception attributes */
    u64 *bb_samples_ptr;                // Location of baseband demodulated samples
    u32 valid_bb_samples;               // Amount of samples copied from FPGA by DMA    
}IQ_Demod_t;

int iq_demod_init(IQ_Demod_t *demod, uint32_t hw_address, uint32_t axi_dma_device_id);

int iq_demod_set_lo(IQ_Demod_t *demod, lo_freq_e freq);

int iq_demod_set_decimation(IQ_Demod_t *demod, rates_e rate);

int iq_demod_trigger_transfer(IQ_Demod_t *demod, uint32_t num_samples);

void iq_demod_set_source_external(IQ_Demod_t* demod);

void iq_demod_set_source_internal(IQ_Demod_t* demod);

void iq_demod_start(IQ_Demod_t *demod);

void iq_demod_stop(IQ_Demod_t *demod);

void iq_demod_get_i_samples(IQ_Demod_t *demod, s32 *i_samples, u32 num_samples);

void iq_demod_get_q_samples(IQ_Demod_t *demod, s32 *q_samples, u32 num_samples);

#endif
