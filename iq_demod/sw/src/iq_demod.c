#include "iq_demod.h"
#include "reg_ops.h"

#ifdef OS_FREERTOS
    #include "FreeRTOS.h"
    #include "task.h"
#endif

/* Buffer to store transfered samples */
static u64 demod_samples[MAX_TRANSFER_SAMPLES + 4] __attribute__ ((aligned (64)));

/**
 * @brief Maps an instance of an iq_demodlator to an address location
 * 
 * @param demod IQ Demodulator instance
 * @param hw_address Hardware address of memory mapped AXI iq_demod configuration registers
 * @param axi_dma_device_id Hardware address of memory mapped AXI-DMA IP Core for baseband samples transfer
 */
int iq_demod_init(IQ_Demod_t *demod, uint32_t hw_address, uint32_t axi_dma_device_id){
    
    /* Initially, set everything to NULL */
    memset(demod,0,sizeof(IQ_Demod_t));
    
    demod->address = hw_address;
    demod->axi_dma_device_id = axi_dma_device_id;
    demod->bb_samples_ptr = demod_samples;

    /* Init Debug Vector */
	memset(demod_samples,0,MAX_TRANSFER_BYTES);

    int status;
    /* Init DMA */
    demod->axi_dma_cfg_ptr = XAxiDma_LookupConfig(demod->axi_dma_device_id);
    if (!demod->axi_dma_cfg_ptr) {
		return -1;
    }
    
    status = XAxiDma_CfgInitialize(&demod->axi_dma_inst, demod->axi_dma_cfg_ptr);
	if (status != SUCCESS) {
		return -1;
	}

    /* Not working with interrupts yet. Not neccessary */
    XAxiDma_IntrDisable(&demod->axi_dma_inst, XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DEVICE_TO_DMA);
    
    return status;
}

/**
 * @brief Sets local oscillator frequency for demodulation
 * 
 * @param demod IQ Demodulator instance
 * @param freq Frequency value from possible enumerated values
 * @return int -1 on ERROR, 0 on SUCCESS
 */
int iq_demod_set_lo(IQ_Demod_t *demod, lo_freq_e freq){
    uint32_t pinc_val;
    int retval = 0;

    iq_demod_stop(demod);

    switch (freq){
    case LOCAL_OSC_10M:
        demod->lo_freq_khz = freq;
        break;
    case LOCAL_OSC_20M:
        demod->lo_freq_khz = freq;
        break;
    case LOCAL_OSC_30M:
        demod->lo_freq_khz = freq;
        break;
    case LOCAL_OSC_40M:
        demod->lo_freq_khz = freq;
        break;
    default:
        retval = -1;
        break;
    }

    if (0 == retval){
        pinc_val = (((uint32_t) demod->lo_freq_khz) * ((1U << LO_PINC_BITS) / FCLK_KHZ)) & LO_PINC_MASK;
        _writeReg(demod->address + REG_1_OFFSET, pinc_val);
    }

    return retval;
}

/**
 * @brief Sets decimation rate of demodulator instance.
 * 
 * @param demod IQ Demodulator instance
 * @param rate Decimation rate value from possible enumerated values
 * @return int -1 on ERROR, 0 on SUCCESS
 */
int iq_demod_set_decimation(IQ_Demod_t *demod, rates_e rate){
    int retval = 0;

    iq_demod_stop(demod);

    if(rate == RATE_4 || rate == RATE_5 || rate == RATE_8 || rate == RATE_16 || rate == RATE_40 || rate == RATE_70 ){
        demod->decimation_rate = rate;
        _writeReg(demod->address + REG_2_OFFSET, (uint32_t) rate);
    }
    else{
        retval = -1;
    }
    return retval;
}

/**
 * @brief Triggers transfer of samples from FPGA to PS
 * 
 * @param demod IQ Demodulator instance
 * @param num_samples Amount of samples to transfer
 * @return int -1 on ERROR, 0 on SUCCESS
 */
int iq_demod_trigger_transfer(IQ_Demod_t *demod, uint32_t num_samples){

    int retval = 0;
    
    if ((num_samples <= MAX_TRANSFER_SAMPLES) & (demod->enabled))
    {   
        /* Write amount of samples to Hw */ 
        demod->num_samples = num_samples;
        _writeReg(demod->address + REG_3_OFFSET, demod->num_samples);
        
        Xil_DCacheFlushRange((UINTPTR)demod_samples, MAX_TRANSFER_BYTES);

        /* Enable demodulator transfer. This enables packet counter */
        _writeBit(demod->address + REG_0_OFFSET, TRANSFER_EN_BIT, TRUE);

        /* Start DMA transfer */
        int Status = XAxiDma_SimpleTransfer(&demod->axi_dma_inst,(UINTPTR)demod_samples, (demod->num_samples + 6) * sizeof(u64), XAXIDMA_DEVICE_TO_DMA);

        if (Status != XST_SUCCESS) {
            retval = -1;
        }
        
        /* Wait and poll for DMA transfer end */
        while (retval == 0 && (XAxiDma_Busy(&demod->axi_dma_inst, XAXIDMA_DEVICE_TO_DMA)))
        {
        	//TODO: Exit after some time and return error
            /* Wait 10 ms*/
            #ifdef OS_FREERTOS
//                vTaskDelay(pdMS_TO_TICKS( 10 ));
            #endif
        } 
        
        /* Read how many bytes were transfered by DMA*/
        u32 buffLen = XAxiDma_ReadReg(demod->axi_dma_inst.RegBase + (XAXIDMA_RX_OFFSET * XAXIDMA_DEVICE_TO_DMA), XAXIDMA_BUFFLEN_OFFSET);
        
        /* Transform number of bytes, to number of 64bit samples */
        /* Baseband samples are 64 bit words with i and q samples in lower and higher 32 bits respectively */
        demod->valid_bb_samples = buffLen / sizeof(u64);
        if (demod->valid_bb_samples == 0){
            retval = -1;
        }
    }
    else
    {
        retval = -1;
    }
    
	return retval;

}

/**
 * @brief Sets demodulator source to external signals
 * 
 * @param demod IQ Demodulator instance
 */
void iq_demod_set_source_external(IQ_Demod_t* demod){
    demod->source = EXTERNAL;
    _writeBit(demod->address + REG_0_OFFSET, SOURCE_BIT, EXTERNAL);
}

/**
 * @brief Sets demodulator source to internally generated signals
 * 
 * @param demod IQ Demodulator instance
 */
void iq_demod_set_source_internal(IQ_Demod_t* demod){
    demod->source = INTERNAL;
    _writeBit(demod->address + REG_0_OFFSET, SOURCE_BIT, INTERNAL);
}

/**
 * @brief Starts demodulator operation
 * 
 * @param demod IQ Demodulator instance
 */
void iq_demod_start(IQ_Demod_t *demod){
    
    demod->enabled = 1;

    _writeBit(demod->address + REG_0_OFFSET, ENABLE_BIT, TRUE);
}

/**
 * @brief Stops demodulator operation
 * 
 * @param demod IQ Demodulator instance
 */
void iq_demod_stop(IQ_Demod_t *demod){
    
    demod->enabled = 1;

    _writeBit(demod->address + REG_0_OFFSET, ENABLE_BIT, FALSE);
}

/**
 * @brief Copies num_samples of I channel samples to i_samples array
 * 
 * @param demod IQ Demodulator instance
 * @param i_samples Destination array
 * @param num_samples Amount of samples to copy
 */
void iq_demod_get_i_samples(IQ_Demod_t *demod, s32 *i_samples, u32 num_samples){
    
    /* Raw samples are in demod_samples global buffer */
	s32 *samples = (s32*) demod_samples;

    for (u32 i = 0; i < num_samples ; i++){
        i_samples[i] = (s32) (samples[i*2]);
    } 
}

/**
 * @brief Copies num_samples of Q channel samples to i_samples array
 * 
 * @param demod IQ Demodulator instance
 * @param i_samples Destination array
 * @param num_samples Amount of samples to copy
 */
void iq_demod_get_q_samples(IQ_Demod_t *demod, s32 *q_samples, u32 num_samples){

    /* Raw samples are in demod_samples global buffer*/
	s32 *samples = (s32*) demod_samples;
    /* Shift 32 bits */ 
    samples++;

    for (u32 i = 0; i < num_samples ; i++){
        q_samples[i] = (s32) (samples[i*2]);
    } 
}
