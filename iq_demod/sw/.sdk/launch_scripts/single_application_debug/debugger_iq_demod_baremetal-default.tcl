connect -url tcp:127.0.0.1:3121
targets -set -nocase -filter {name =~"APU*"}
rst -system
after 3000
targets -set -filter {jtag_cable_name =~ "Digilent Arty Z7 003017A4C943A" && level==0} -index 1
fpga -file /mnt/Archivos/cese/trabajo_final/iq_demod/sw/iq_demod_baremetal/_ide/bitstream/iq_demod_bd_wrapper.bit
targets -set -nocase -filter {name =~"APU*"}
loadhw -hw /mnt/Archivos/cese/trabajo_final/iq_demod/sw/iq_demod_bd_wrapper/export/iq_demod_bd_wrapper/hw/iq_demod_bd_wrapper_1.xsa -mem-ranges [list {0x40000000 0xbfffffff}]
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*"}
source /mnt/Archivos/cese/trabajo_final/iq_demod/sw/iq_demod_baremetal/_ide/psinit/ps7_init.tcl
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "*A9*#0"}
dow /mnt/Archivos/cese/trabajo_final/iq_demod/sw/iq_demod_baremetal/Debug/iq_demod_baremetal.elf
configparams force-mem-access 0
bpadd -addr &main
