
################################################################
# This is a generated script based on design: axi_regs_tb_bd
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2019.2
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_msg_id "BD_TCL-109" "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source axi_regs_tb_bd_script.tcl


# The design that will be created by this Tcl script contains the following 
# module references:
# mm2iq_config

# Please add the sources of those modules before sourcing this Tcl script.

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xc7z010clg400-1
   set_property BOARD_PART digilentinc.com:arty-z7-10:part0:1.0 [current_project]
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name axi_regs_tb_bd

# This script was generated for a remote BD. To create a non-remote design,
# change the variable <run_remote_bd_flow> to <0>.

set run_remote_bd_flow 1
if { $run_remote_bd_flow == 1 } {
  # Set the reference directory for source file relative paths (by default 
  # the value is script directory path)
  set origin_dir ./bd

  # Use origin directory path location variable, if specified in the tcl shell
  if { [info exists ::origin_dir_loc] } {
     set origin_dir $::origin_dir_loc
  }

  set str_bd_folder [file normalize ${origin_dir}]
  set str_bd_filepath ${str_bd_folder}/${design_name}/${design_name}.bd

  # Check if remote design exists on disk
  if { [file exists $str_bd_filepath ] == 1 } {
     catch {common::send_msg_id "BD_TCL-110" "ERROR" "The remote BD file path <$str_bd_filepath> already exists!"}
     common::send_msg_id "BD_TCL-008" "INFO" "To create a non-remote BD, change the variable <run_remote_bd_flow> to <0>."
     common::send_msg_id "BD_TCL-009" "INFO" "Also make sure there is no design <$design_name> existing in your current project."

     return 1
  }

  # Check if design exists in memory
  set list_existing_designs [get_bd_designs -quiet $design_name]
  if { $list_existing_designs ne "" } {
     catch {common::send_msg_id "BD_TCL-111" "ERROR" "The design <$design_name> already exists in this project! Will not create the remote BD <$design_name> at the folder <$str_bd_folder>."}

     common::send_msg_id "BD_TCL-010" "INFO" "To create a non-remote BD, change the variable <run_remote_bd_flow> to <0> or please set a different value to variable <design_name>."

     return 1
  }

  # Check if design exists on disk within project
  set list_existing_designs [get_files -quiet */${design_name}.bd]
  if { $list_existing_designs ne "" } {
     catch {common::send_msg_id "BD_TCL-112" "ERROR" "The design <$design_name> already exists in this project at location:
    $list_existing_designs"}
     catch {common::send_msg_id "BD_TCL-113" "ERROR" "Will not create the remote BD <$design_name> at the folder <$str_bd_folder>."}

     common::send_msg_id "BD_TCL-011" "INFO" "To create a non-remote BD, change the variable <run_remote_bd_flow> to <0> or please set a different value to variable <design_name>."

     return 1
  }

  # Now can create the remote BD
  # NOTE - usage of <-dir> will create <$str_bd_folder/$design_name/$design_name.bd>
  create_bd_design -dir $str_bd_folder $design_name
} else {

  # Create regular design
  if { [catch {create_bd_design $design_name} errmsg] } {
     common::send_msg_id "BD_TCL-012" "INFO" "Please set a different value to variable <design_name>."

     return 1
  }
}

current_bd_design $design_name

set bCheckIPsPassed 1
##################################################################
# CHECK IPs
##################################################################
set bCheckIPs 1
if { $bCheckIPs == 1 } {
   set list_check_ips "\ 
xilinx.com:ip:axis_broadcaster:1.1\
xilinx.com:ip:axis_combiner:1.1\
xilinx.com:ip:cic_compiler:4.0\
xilinx.com:ip:cmpy:6.0\
xilinx.com:ip:fir_compiler:7.2\
xilinx.com:ip:dds_compiler:6.0\
"

   set list_ips_missing ""
   common::send_msg_id "BD_TCL-006" "INFO" "Checking if the following IPs exist in the project's IP catalog: $list_check_ips ."

   foreach ip_vlnv $list_check_ips {
      set ip_obj [get_ipdefs -all $ip_vlnv]
      if { $ip_obj eq "" } {
         lappend list_ips_missing $ip_vlnv
      }
   }

   if { $list_ips_missing ne "" } {
      catch {common::send_msg_id "BD_TCL-115" "ERROR" "The following IPs are not found in the IP Catalog:\n  $list_ips_missing\n\nResolution: Please add the repository containing the IP(s) to the project." }
      set bCheckIPsPassed 0
   }

}

##################################################################
# CHECK Modules
##################################################################
set bCheckModules 1
if { $bCheckModules == 1 } {
   set list_check_mods "\ 
mm2iq_config\
"

   set list_mods_missing ""
   common::send_msg_id "BD_TCL-006" "INFO" "Checking if the following modules exist in the project's sources: $list_check_mods ."

   foreach mod_vlnv $list_check_mods {
      if { [can_resolve_reference $mod_vlnv] == 0 } {
         lappend list_mods_missing $mod_vlnv
      }
   }

   if { $list_mods_missing ne "" } {
      catch {common::send_msg_id "BD_TCL-115" "ERROR" "The following module(s) are not found in the project: $list_mods_missing" }
      common::send_msg_id "BD_TCL-008" "INFO" "Please add source files for the missing module(s) above."
      set bCheckIPsPassed 0
   }
}

if { $bCheckIPsPassed != 1 } {
  common::send_msg_id "BD_TCL-1003" "WARNING" "Will not continue with creation of design due to the error(s) above."
  return 3
}

##################################################################
# DESIGN PROCs
##################################################################



# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set M_AXIS_OUT [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 M_AXIS_OUT ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {125000000} \
   ] $M_AXIS_OUT

  set S_AXIS_INPUT [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 S_AXIS_INPUT ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {125000000} \
   CONFIG.HAS_TKEEP {0} \
   CONFIG.HAS_TLAST {1} \
   CONFIG.HAS_TREADY {1} \
   CONFIG.HAS_TSTRB {0} \
   CONFIG.LAYERED_METADATA {undef} \
   CONFIG.TDATA_NUM_BYTES {4} \
   CONFIG.TDEST_WIDTH {0} \
   CONFIG.TID_WIDTH {0} \
   CONFIG.TUSER_WIDTH {0} \
   ] $S_AXIS_INPUT

  set S_AXI_IQ_DEMOD_CONFIG [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S_AXI_IQ_DEMOD_CONFIG ]
  set_property -dict [ list \
   CONFIG.ADDR_WIDTH {32} \
   CONFIG.ARUSER_WIDTH {0} \
   CONFIG.AWUSER_WIDTH {0} \
   CONFIG.BUSER_WIDTH {0} \
   CONFIG.DATA_WIDTH {32} \
   CONFIG.FREQ_HZ {125000000} \
   CONFIG.HAS_BRESP {1} \
   CONFIG.HAS_BURST {0} \
   CONFIG.HAS_CACHE {0} \
   CONFIG.HAS_LOCK {0} \
   CONFIG.HAS_PROT {0} \
   CONFIG.HAS_QOS {0} \
   CONFIG.HAS_REGION {0} \
   CONFIG.HAS_RRESP {1} \
   CONFIG.HAS_WSTRB {1} \
   CONFIG.ID_WIDTH {0} \
   CONFIG.MAX_BURST_LENGTH {1} \
   CONFIG.NUM_READ_OUTSTANDING {1} \
   CONFIG.NUM_READ_THREADS {1} \
   CONFIG.NUM_WRITE_OUTSTANDING {1} \
   CONFIG.NUM_WRITE_THREADS {1} \
   CONFIG.PROTOCOL {AXI4LITE} \
   CONFIG.READ_WRITE_MODE {READ_WRITE} \
   CONFIG.RUSER_BITS_PER_BYTE {0} \
   CONFIG.RUSER_WIDTH {0} \
   CONFIG.SUPPORTS_NARROW_BURST {0} \
   CONFIG.WUSER_BITS_PER_BYTE {0} \
   CONFIG.WUSER_WIDTH {0} \
   ] $S_AXI_IQ_DEMOD_CONFIG


  # Create ports
  set aclk [ create_bd_port -dir I -type clk -freq_hz 125000000 aclk ]
  set_property -dict [ list \
   CONFIG.ASSOCIATED_BUSIF {M_AXIS_OUT:S_AXIS_CIC_RATE:S_AXIS_INPUT:S_AXIS_PHASE_LO:S_AXI_IQ_DEMOD_CONFIG} \
 ] $aclk
  set aresetn [ create_bd_port -dir I -type rst aresetn ]

  # Create instance: axis_broadcaster_cic_config, and set properties
  set axis_broadcaster_cic_config [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_broadcaster:1.1 axis_broadcaster_cic_config ]
  set_property -dict [ list \
   CONFIG.M00_TDATA_REMAP {tdata[7:0]} \
   CONFIG.M01_TDATA_REMAP {tdata[7:0]} \
   CONFIG.M_TDATA_NUM_BYTES {1} \
 ] $axis_broadcaster_cic_config

  # Create instance: axis_broadcaster_mixer, and set properties
  set axis_broadcaster_mixer [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_broadcaster:1.1 axis_broadcaster_mixer ]
  set_property -dict [ list \
   CONFIG.M00_TDATA_REMAP {tdata[31:0]} \
   CONFIG.M01_TDATA_REMAP {tdata[63:32]} \
   CONFIG.M_TDATA_NUM_BYTES {4} \
   CONFIG.S_TDATA_NUM_BYTES {8} \
 ] $axis_broadcaster_mixer

  # Create instance: axis_combiner_0, and set properties
  set axis_combiner_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_combiner:1.1 axis_combiner_0 ]
  set_property -dict [ list \
   CONFIG.TDATA_NUM_BYTES {4} \
 ] $axis_combiner_0

  # Create instance: cic_compiler_i, and set properties
  set cic_compiler_i [ create_bd_cell -type ip -vlnv xilinx.com:ip:cic_compiler:4.0 cic_compiler_i ]
  set_property -dict [ list \
   CONFIG.Clock_Frequency {125} \
   CONFIG.Filter_Type {Decimation} \
   CONFIG.HAS_ARESETN {true} \
   CONFIG.Input_Data_Width {29} \
   CONFIG.Input_Sample_Frequency {125} \
   CONFIG.Maximum_Rate {70} \
   CONFIG.Minimum_Rate {4} \
   CONFIG.Number_Of_Channels {1} \
   CONFIG.Number_Of_Stages {3} \
   CONFIG.Output_Data_Width {32} \
   CONFIG.Quantization {Truncation} \
   CONFIG.RateSpecification {Frequency_Specification} \
   CONFIG.SamplePeriod {1} \
   CONFIG.Sample_Rate_Changes {Programmable} \
 ] $cic_compiler_i

  # Create instance: cic_compiler_q, and set properties
  set cic_compiler_q [ create_bd_cell -type ip -vlnv xilinx.com:ip:cic_compiler:4.0 cic_compiler_q ]
  set_property -dict [ list \
   CONFIG.Clock_Frequency {125} \
   CONFIG.Filter_Type {Decimation} \
   CONFIG.HAS_ARESETN {true} \
   CONFIG.Input_Data_Width {29} \
   CONFIG.Input_Sample_Frequency {125} \
   CONFIG.Maximum_Rate {70} \
   CONFIG.Minimum_Rate {4} \
   CONFIG.Number_Of_Channels {1} \
   CONFIG.Number_Of_Stages {3} \
   CONFIG.Output_Data_Width {32} \
   CONFIG.Quantization {Truncation} \
   CONFIG.RateSpecification {Frequency_Specification} \
   CONFIG.SamplePeriod {1} \
   CONFIG.Sample_Rate_Changes {Programmable} \
 ] $cic_compiler_q

  # Create instance: cmpy, and set properties
  set cmpy [ create_bd_cell -type ip -vlnv xilinx.com:ip:cmpy:6.0 cmpy ]
  set_property -dict [ list \
   CONFIG.ACLKEN {true} \
   CONFIG.APortWidth {14} \
   CONFIG.ARESETN {true} \
   CONFIG.BPortWidth {14} \
   CONFIG.FlowControl {Blocking} \
   CONFIG.HasATLAST {true} \
   CONFIG.HasBTLAST {true} \
   CONFIG.MinimumLatency {7} \
   CONFIG.OptimizeGoal {Performance} \
   CONFIG.OutTLASTBehv {Pass_A_TLAST} \
   CONFIG.OutputWidth {29} \
 ] $cmpy

  # Create instance: fir_compiler, and set properties
  set fir_compiler [ create_bd_cell -type ip -vlnv xilinx.com:ip:fir_compiler:7.2 fir_compiler ]
  set_property -dict [ list \
   CONFIG.Channel_Sequence {Basic} \
   CONFIG.CoefficientVector {2,-5,4,-7,7,-12,14,-21,26,-38,48,-65,84,-112,146,-198,265,-367,516,-761,1179,-1995,3834,-9199,32767,-9199,3834,-1995,1179,-761,516,-367,265,-198,146,-112,84,-65,48,-38,26,-21,14,-12,7,-7,4,-5,2} \
   CONFIG.Coefficient_Fractional_Bits {0} \
   CONFIG.Coefficient_Sets {1} \
   CONFIG.Coefficient_Sign {Signed} \
   CONFIG.Coefficient_Structure {Inferred} \
   CONFIG.Coefficient_Width {16} \
   CONFIG.ColumnConfig {7} \
   CONFIG.DATA_Has_TLAST {Packet_Framing} \
   CONFIG.Data_Width {32} \
   CONFIG.Filter_Architecture {Systolic_Multiply_Accumulate} \
   CONFIG.M_DATA_Has_TUSER {Not_Required} \
   CONFIG.Multi_Column_Support {Automatic} \
   CONFIG.Number_Channels {1} \
   CONFIG.Number_Paths {2} \
   CONFIG.Optimization_Goal {Area} \
   CONFIG.Optimization_List {None} \
   CONFIG.Optimization_Selection {None} \
   CONFIG.Output_Rounding_Mode {Truncate_LSBs} \
   CONFIG.Output_Width {32} \
   CONFIG.Quantization {Integer_Coefficients} \
   CONFIG.RateSpecification {Input_Sample_Period} \
   CONFIG.S_DATA_Has_TUSER {Not_Required} \
   CONFIG.SamplePeriod {4} \
   CONFIG.Select_Pattern {All} \
 ] $fir_compiler

  # Create instance: local_oscillator, and set properties
  set local_oscillator [ create_bd_cell -type ip -vlnv xilinx.com:ip:dds_compiler:6.0 local_oscillator ]
  set_property -dict [ list \
   CONFIG.DATA_Has_TLAST {Not_Required} \
   CONFIG.DDS_Clock_Rate {125} \
   CONFIG.Frequency_Resolution {0.4} \
   CONFIG.Has_ACLKEN {true} \
   CONFIG.Has_ARESETn {true} \
   CONFIG.Has_Phase_Out {false} \
   CONFIG.Has_TREADY {true} \
   CONFIG.Latency {13} \
   CONFIG.M_DATA_Has_TUSER {Not_Required} \
   CONFIG.Negative_Sine {true} \
   CONFIG.Noise_Shaping {None} \
   CONFIG.Output_Frequency1 {0} \
   CONFIG.Output_Width {14} \
   CONFIG.PINC1 {0} \
   CONFIG.Parameter_Entry {Hardware_Parameters} \
   CONFIG.Phase_Increment {Streaming} \
   CONFIG.Phase_Width {30} \
   CONFIG.S_PHASE_Has_TUSER {Not_Required} \
 ] $local_oscillator

  # Create instance: mm2iq_config, and set properties
  set block_name mm2iq_config
  set block_cell_name mm2iq_config
  if { [catch {set mm2iq_config [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_msg_id "BD_TCL-105" "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $mm2iq_config eq "" } {
     catch {common::send_msg_id "BD_TCL-106" "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }
  
  # Create interface connections
  connect_bd_intf_net -intf_net S_AXIS_A_0_1 [get_bd_intf_ports S_AXIS_INPUT] [get_bd_intf_pins cmpy/S_AXIS_A]
  connect_bd_intf_net -intf_net S_AXI_0_1 [get_bd_intf_ports S_AXI_IQ_DEMOD_CONFIG] [get_bd_intf_pins mm2iq_config/S_AXI]
  connect_bd_intf_net -intf_net axis_broadcaster_0_M00_AXIS [get_bd_intf_pins axis_broadcaster_mixer/M00_AXIS] [get_bd_intf_pins cic_compiler_i/S_AXIS_DATA]
  connect_bd_intf_net -intf_net axis_broadcaster_0_M01_AXIS [get_bd_intf_pins axis_broadcaster_mixer/M01_AXIS] [get_bd_intf_pins cic_compiler_q/S_AXIS_DATA]
  connect_bd_intf_net -intf_net axis_broadcaster_cic_config_M00_AXIS [get_bd_intf_pins axis_broadcaster_cic_config/M00_AXIS] [get_bd_intf_pins cic_compiler_q/S_AXIS_CONFIG]
  connect_bd_intf_net -intf_net axis_broadcaster_cic_config_M01_AXIS [get_bd_intf_pins axis_broadcaster_cic_config/M01_AXIS] [get_bd_intf_pins cic_compiler_i/S_AXIS_CONFIG]
  connect_bd_intf_net -intf_net axis_combiner_0_M_AXIS [get_bd_intf_pins axis_combiner_0/M_AXIS] [get_bd_intf_pins fir_compiler/S_AXIS_DATA]
  connect_bd_intf_net -intf_net cic_compiler_i_M_AXIS_DATA [get_bd_intf_pins axis_combiner_0/S00_AXIS] [get_bd_intf_pins cic_compiler_i/M_AXIS_DATA]
  connect_bd_intf_net -intf_net cic_compiler_q_M_AXIS_DATA [get_bd_intf_pins axis_combiner_0/S01_AXIS] [get_bd_intf_pins cic_compiler_q/M_AXIS_DATA]
  connect_bd_intf_net -intf_net cmpy_M_AXIS_DOUT [get_bd_intf_pins axis_broadcaster_mixer/S_AXIS] [get_bd_intf_pins cmpy/M_AXIS_DOUT]
  connect_bd_intf_net -intf_net dds_compiler_0_M_AXIS_DATA [get_bd_intf_pins cmpy/S_AXIS_B] [get_bd_intf_pins local_oscillator/M_AXIS_DATA]
  connect_bd_intf_net -intf_net fir_compiler_M_AXIS_DATA [get_bd_intf_ports M_AXIS_OUT] [get_bd_intf_pins fir_compiler/M_AXIS_DATA]
  connect_bd_intf_net -intf_net mm2iq_config_0_S_AXIS_CIC_RATE [get_bd_intf_pins axis_broadcaster_cic_config/S_AXIS] [get_bd_intf_pins mm2iq_config/S_AXIS_CIC_RATE]
  connect_bd_intf_net -intf_net mm2iq_config_0_S_AXIS_PHASE_LO [get_bd_intf_pins local_oscillator/S_AXIS_PHASE] [get_bd_intf_pins mm2iq_config/S_AXIS_PHASE_LO]

  # Create port connections
  connect_bd_net -net aclk_1 [get_bd_ports aclk] [get_bd_pins axis_broadcaster_cic_config/aclk] [get_bd_pins axis_broadcaster_mixer/aclk] [get_bd_pins axis_combiner_0/aclk] [get_bd_pins cic_compiler_i/aclk] [get_bd_pins cic_compiler_q/aclk] [get_bd_pins cmpy/aclk] [get_bd_pins fir_compiler/aclk] [get_bd_pins local_oscillator/aclk] [get_bd_pins mm2iq_config/S_AXI_CLK]
  connect_bd_net -net aclken_1 [get_bd_pins cmpy/aclken] [get_bd_pins local_oscillator/aclken] [get_bd_pins mm2iq_config/iq_en_o]
  connect_bd_net -net aresetn_1 [get_bd_ports aresetn] [get_bd_pins axis_broadcaster_cic_config/aresetn] [get_bd_pins axis_broadcaster_mixer/aresetn] [get_bd_pins axis_combiner_0/aresetn] [get_bd_pins cic_compiler_i/aresetn] [get_bd_pins cic_compiler_q/aresetn] [get_bd_pins cmpy/aresetn] [get_bd_pins local_oscillator/aresetn] [get_bd_pins mm2iq_config/S_AXI_ARESETN]

  # Create address segments
  assign_bd_address -offset 0x00000000 -range 0x00000400 -target_address_space [get_bd_addr_spaces S_AXI_IQ_DEMOD_CONFIG] [get_bd_addr_segs mm2iq_config/S_AXI/reg0] -force


  # Restore current instance
  current_bd_instance $oldCurInst

  validate_bd_design
  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


