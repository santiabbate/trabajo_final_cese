`timescale 1ns / 1ps
/**
 * @file axi_iq_config_complete_tb.sv
 * @author Santiago Abbate
 * @brief CESE - Trabajo Final - Control de etapa digital de RADAR pulsado multipropósito.
 * Registers + Mixer + CIC + FIR complete chain testbench. Input signal from matlab generated samples.
 */

module axi_iq_config_complete_tb();
    localparam CLK = 8;
    localparam PINC_BITS = 30;
    parameter DIGITAL_WIDTH = 14;

    /**
     * Signals
     */

    /* AXI Bus */
    // ### AXI4-lite slave signals #########
    // *** Write address signals ***
    logic         S_AXI_IQ_DEMOD_CONFIG_awready;
    logic   [31:0] S_AXI_IQ_DEMOD_CONFIG_awaddr;
    logic          S_AXI_IQ_DEMOD_CONFIG_awvalid;
    // *** Write data signals ***
    logic         S_AXI_IQ_DEMOD_CONFIG_wready;
    logic   [31:0] S_AXI_IQ_DEMOD_CONFIG_wdata;
    logic   [3:0]  S_AXI_IQ_DEMOD_CONFIG_wstrb;
    logic          S_AXI_IQ_DEMOD_CONFIG_wvalid;
    // *** Write response signals ***
    logic          S_AXI_IQ_DEMOD_CONFIG_bready;
    logic  [1:0]  S_AXI_IQ_DEMOD_CONFIG_bresp;
    logic         S_AXI_IQ_DEMOD_CONFIG_bvalid;
    // *** Read address signals ***
    logic         S_AXI_IQ_DEMOD_CONFIG_arready;
    logic   [31:0] S_AXI_IQ_DEMOD_CONFIG_araddr;
    logic          S_AXI_IQ_DEMOD_CONFIG_arvalid;
    // *** Read data signals ***	
    logic          S_AXI_IQ_DEMOD_CONFIG_rready;
    logic  [31:0] S_AXI_IQ_DEMOD_CONFIG_rdata;
    logic  [1:0]  S_AXI_IQ_DEMOD_CONFIG_rresp;
    logic         S_AXI_IQ_DEMOD_CONFIG_rvalid;

    // Out signals (From FIR Compiler)
    logic [63:0]M_AXIS_OUT_tdata;
    logic M_AXIS_OUT_tlast;
    logic M_AXIS_OUT_tvalid;

    // Input sine signals
    logic [31:0] S_AXIS_INPUT_tdata;
    logic S_AXIS_INPUT_tlast = 0;
    logic S_AXIS_INPUT_tready;
    logic S_AXIS_INPUT_tvalid;

    // DDS phase
    logic [31:0] S_AXIS_DDS_PHASE_LO;

    /* File descriptors */
    int fd;
    int fi;
    int fp;

    /* DDS Configuration helper function */
    function automatic void dds_set_cont_freq(input int unsigned fout_MHz);
        automatic int unsigned pinc; 
        pinc = fout_MHz * ((2 ** PINC_BITS) / 125);
        S_AXIS_DDS_PHASE_LO = pinc;
    endfunction;

    real lo = 0;
    int rate = 4;
    logic [DIGITAL_WIDTH -1 : 0] test_chirp_discrete;

    /**
     * Clock & Reset
     */

    logic aclk = 0;
    logic aclken = 1;
    logic aresetn = 0;
    always #(CLK/2) aclk = !aclk;
    // always aclken = 1;
    initial #50 aresetn = 1;

    /* Parameters loading */
    initial begin
        /* Read params of simulated input signal. Local oscillator, and rate conversion (Rate is function of bandwith of signal) */
        fp = $fopen ("../../../../../sim/m/params.txt", "r");
        $fscanf(fp,"%f,%d",lo,rate);
    end

    assign S_AXIS_INPUT_tdata = {{16{1'b0}},{2{test_chirp_discrete[13]}},test_chirp_discrete};

    /* DUT instance */
    axi_regs_tb_bd_wrapper dut(
        .M_AXIS_OUT_tdata,
        .M_AXIS_OUT_tlast,
        .M_AXIS_OUT_tvalid,
        .S_AXIS_INPUT_tdata,
        .S_AXIS_INPUT_tlast,
        .S_AXIS_INPUT_tready,
        .S_AXIS_INPUT_tvalid,
        .S_AXI_IQ_DEMOD_CONFIG_araddr,
        .S_AXI_IQ_DEMOD_CONFIG_arready,
        .S_AXI_IQ_DEMOD_CONFIG_arvalid,
        .S_AXI_IQ_DEMOD_CONFIG_awaddr,
        .S_AXI_IQ_DEMOD_CONFIG_awready,
        .S_AXI_IQ_DEMOD_CONFIG_awvalid,
        .S_AXI_IQ_DEMOD_CONFIG_bready,
        .S_AXI_IQ_DEMOD_CONFIG_bresp,
        .S_AXI_IQ_DEMOD_CONFIG_bvalid,
        .S_AXI_IQ_DEMOD_CONFIG_rdata,
        .S_AXI_IQ_DEMOD_CONFIG_rready,
        .S_AXI_IQ_DEMOD_CONFIG_rresp,
        .S_AXI_IQ_DEMOD_CONFIG_rvalid,
        .S_AXI_IQ_DEMOD_CONFIG_wdata,
        .S_AXI_IQ_DEMOD_CONFIG_wready,
        .S_AXI_IQ_DEMOD_CONFIG_wstrb,
        .S_AXI_IQ_DEMOD_CONFIG_wvalid,
        .aclk,
        .aresetn);

    /* Main test */
    initial begin
        dds_set_cont_freq(lo);
        /* Open file for results dump */
        fd = $fopen ("../../../../../sim/m/fir/dump.txt", "w");
        /* Open source signal file */
        fi = $fopen ("../../../../../sim/m/chirps.txt", "r");
        
        /* AXI4 Initialization*/
        S_AXI_IQ_DEMOD_CONFIG_awaddr = 0;
        S_AXI_IQ_DEMOD_CONFIG_awvalid = 0;
        S_AXI_IQ_DEMOD_CONFIG_wdata = 0;
        S_AXI_IQ_DEMOD_CONFIG_wdata = 0;
        S_AXI_IQ_DEMOD_CONFIG_wstrb = 0;
        S_AXI_IQ_DEMOD_CONFIG_wvalid = 0;
        S_AXI_IQ_DEMOD_CONFIG_bready = 1;
        S_AXI_IQ_DEMOD_CONFIG_araddr = 0;
        S_AXI_IQ_DEMOD_CONFIG_arvalid = 0;
        S_AXI_IQ_DEMOD_CONFIG_rready = 1;

        /* Validate input signal */
        S_AXIS_INPUT_tvalid = 1;

        #50
        /* Enable demodulator */
        axi_write(8'h00, 1);

        #50ns
        /* Local oscillator */
        axi_write(8'h04, S_AXIS_DDS_PHASE_LO);
        #50ns
        /* Decimator rate change */
        axi_write(8'h08, rate);

        #500us

        $fclose(fd);
        $fclose(fi);
        $finish;
    end

    /**
     * Signal dumps to file
     */
    logic signed [13:0] sampled_test_signal;
    logic signed [31:0] sampled_cmpy_out_i;
    logic signed [31:0] sampled_cmpy_out_q;
    logic signed [31:0] sampled_cic_out_i;
    logic signed [31:0] sampled_cic_out_q;
    logic signed [31:0] sampled_fir_i_wave;
    logic signed [31:0] sampled_fir_q_wave;

    always @(posedge aclk)
    begin
        if(aresetn)
        begin
            /* Read a new sample of input signal each clock cycle */
            $fscanf (fi, "%d", test_chirp_discrete);
            /* Dump signals to file */
            sampled_test_signal <= test_chirp_discrete;
            sampled_cmpy_out_i <= dut.axi_regs_tb_bd_i.cmpy_M_AXIS_DOUT_TDATA[31:0];
            sampled_cmpy_out_q <= dut.axi_regs_tb_bd_i.cmpy_M_AXIS_DOUT_TDATA[63:32];
            sampled_cic_out_i <= dut.axi_regs_tb_bd_i.cic_compiler_i_M_AXIS_DATA_TDATA;
            sampled_cic_out_q <= dut.axi_regs_tb_bd_i.cic_compiler_q_M_AXIS_DATA_TDATA;
            sampled_fir_i_wave <= M_AXIS_OUT_tdata[31:0];
            sampled_fir_q_wave <= M_AXIS_OUT_tdata[63:32];
            $fdisplay(fd,"%d,%d,%d,%d,%d,%d,%d",sampled_test_signal,sampled_cmpy_out_i,sampled_cmpy_out_q,sampled_cic_out_i,sampled_cic_out_q,sampled_fir_i_wave,sampled_fir_q_wave);
        end
    end

    task axi_write;
        input [31:0] awaddr;
        input [31:0] wdata; 
        begin
            // *** Write address ***
            S_AXI_IQ_DEMOD_CONFIG_awaddr = awaddr;
            S_AXI_IQ_DEMOD_CONFIG_awvalid = 1;
            #CLK;
            S_AXI_IQ_DEMOD_CONFIG_awvalid = 0;
            // *** Write data ***
            S_AXI_IQ_DEMOD_CONFIG_wdata = wdata;
            S_AXI_IQ_DEMOD_CONFIG_wstrb = 4'hf;
            S_AXI_IQ_DEMOD_CONFIG_wvalid = 1; 
            #CLK;
            S_AXI_IQ_DEMOD_CONFIG_wvalid = 0;
            #CLK;
        end
    endtask

    task axi_read;
        input [31:0] araddr; 
        begin
            // *** Write address ***
            S_AXI_IQ_DEMOD_CONFIG_araddr = araddr;
            S_AXI_IQ_DEMOD_CONFIG_arvalid = 1;
            #CLK;
            S_AXI_IQ_DEMOD_CONFIG_arvalid = 0;
            // *** Write data ***
            #CLK;
        end
    endtask
    

endmodule