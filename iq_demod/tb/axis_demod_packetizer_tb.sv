`timescale 1ns / 1ps
/**
 * @file axis_demod_packetizer_tb.sv
 * @author Santiago Abbate
 * @brief CESE - Trabajo Final - Control de etapa digital de RADAR pulsado multipropósito.
 * AXI-Stream demodulatior packetizer testbench
 */

module axis_demod_packetizer_tb();
    localparam CLK = 8;

    /**
     * Signals
     */

    /* AXI-Stream input signals */
    logic [63:0] s_axis_input_tdata = 0;
    logic s_axis_input_tvalid;
    logic s_axis_input_tready;

    /* AXI-Stream output signals */
    logic [63:0] m_axis_output_tdata;
    logic m_axis_output_tvalid;
    logic m_axis_output_tready;
    logic m_axis_output_tlast;

    /* Packet length configuration */
    logic [31:0] packet_length_i;
    
    /**
     * Clock & Reset
     */

    logic aclk = 0;
    logic aclken = 1;
    logic aresetn = 0;
    always #(CLK/2) aclk = !aclk;
    initial #20 aresetn = 1;

    /* Tvalid generation depending on rate change */
    localparam rate = 4;
    int count = 0;
    always
    begin
        #CLK
        if (count == rate) begin
            count = 0;
            s_axis_input_tvalid = 1;
        end
        else begin
            count++;
            s_axis_input_tvalid = 0;
        end
    end

    /* Simulated input data is a ramp */
    always
    begin
        #CLK
        s_axis_input_tdata++;
    end

    /**
     * Main test
     */
    initial begin
        packet_length_i = 10;
        /* Simulate not ready DMA */
        m_axis_output_tready = 0;

        #(CLK * 20)

        /* Simulate ready DMA */
        m_axis_output_tready = 1;

        #1us
        $finish;
    end

    /**
     * DUT
     */

    axis_demod_packetizer dut(
        .aclk,
        .aclken,
        .aresetn,
        .s_axis_input_tdata,
        .s_axis_input_tvalid,
        .s_axis_input_tready,
        .m_axis_output_tdata,
        .m_axis_output_tvalid,
        .m_axis_output_tready,
        .m_axis_output_tlast,
        .packet_length_i
);

endmodule