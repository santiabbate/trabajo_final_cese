`timescale 1ns / 1ps
/**
 * @file fir_chirp_tb.sv
 * @author Santiago Abbate
 * @brief CESE - Trabajo Final - Control de etapa digital de RADAR pulsado multipropósito.
 * Mixer + CIC + FIR Compiler testbench with matlab generated chirps
 */

module fir_chirp_tb();
    localparam CLK = 8;
    localparam PINC_BITS = 30;

    /**
     * Signals
     */

    // Out signals (From FIR Compiler)
    logic [63:0]M_AXIS_OUT_tdata;
    logic M_AXIS_OUT_tlast;
    logic M_AXIS_OUT_tvalid;

    /* CIC Compiler configuration signals */
    logic [31:0] S_AXIS_CIC_RATE_tdata;
    logic S_AXIS_CIC_RATE_tready;
    logic S_AXIS_CIC_RATE_tvalid;

    // DDS configuration signals
    logic [31:0] S_AXIS_PHASE_LO_tdata;
    logic S_AXIS_PHASE_LO_tready;
    logic S_AXIS_PHASE_LO_tvalid;

    // Input sine signals
    logic [31:0] S_AXIS_INPUT_tdata;
    logic S_AXIS_INPUT_tlast;
    logic S_AXIS_INPUT_tready;
    logic S_AXIS_INPUT_tvalid;

    /* File descriptors */
    int fd;
    int fi;
    int fp;

    parameter DIGITAL_WIDTH = 14;

    /* DDS Configuration helper function */
    function automatic void dds_set_cont_freq(input int unsigned fout_MHz);
        automatic int unsigned pinc; 
        pinc = fout_MHz * ((2 ** PINC_BITS) / 125);
        S_AXIS_PHASE_LO_tdata = pinc;
    endfunction;

    real lo = 0;
    int rate = 4;

    logic [DIGITAL_WIDTH -1 : 0] test_chirp_discrete;

    /**
     * Clock & Reset
     */

    logic aclk = 0;
    logic aclken = 1;
    logic aresetn = 0;
    always #(CLK/2) aclk = !aclk;
    // always aclken = 1;
    initial #20 aresetn = 1;

    /* Local oscillator and decimator rate config */
    initial begin
        /* Read params of simulated input signal. Local oscillator, and rate conversion (Rate is function of bandwith of signal) */
        fp = $fopen ("../../../../../sim/m/params.txt", "r");
        $fscanf(fp,"%f,%d",lo,rate);

        // Configure DDS
        dds_set_cont_freq(lo);
        S_AXIS_PHASE_LO_tvalid = 1;
        S_AXIS_INPUT_tvalid = 1;

        /* Configure CIC Compiler Rate */
        S_AXIS_CIC_RATE_tvalid = 0;
        /* Wait for 10 cycles */
        #(10*CLK)
        S_AXIS_CIC_RATE_tdata = rate;
        S_AXIS_CIC_RATE_tvalid = 1;
        #CLK
        S_AXIS_CIC_RATE_tvalid = 0;
    end

    assign S_AXIS_INPUT_tdata = {{16{0}},{2{test_chirp_discrete[13]}},test_chirp_discrete};

    /* DUT instance */
    fir_tb_bd_wrapper dut(
        .M_AXIS_OUT_tdata,
        .M_AXIS_OUT_tlast,
        .M_AXIS_OUT_tvalid,
        .S_AXIS_INPUT_tdata,
        .S_AXIS_INPUT_tlast,
        .S_AXIS_INPUT_tready,
        .S_AXIS_INPUT_tvalid,
        .S_AXIS_PHASE_LO_tdata,
        .S_AXIS_PHASE_LO_tready,
        .S_AXIS_PHASE_LO_tvalid,
        .S_AXIS_CIC_RATE_tdata,
        .S_AXIS_CIC_RATE_tready,
        .S_AXIS_CIC_RATE_tvalid,
        .aclk,
        .aclken,
        .aresetn
        );

    /* Main test */
    initial begin

        /* Open file for results dump */
        fd = $fopen ("../../../../../sim/m/fir/dump.txt", "w");
        /* Open source signal file */
        fi = $fopen ("../../../../../sim/m/chirps.txt", "r");
        
        #150us

        $fclose(fd);
        $fclose(fi);
        $finish;
    end

    /**
     * Signal dumps to file
     */
    logic signed [13:0] sampled_test_signal;
    logic signed [31:0] sampled_cmpy_out_i;
    logic signed [31:0] sampled_cmpy_out_q;
    logic signed [31:0] sampled_cic_out_i;
    logic signed [31:0] sampled_cic_out_q;
    logic signed [31:0] sampled_fir_i_wave;
    logic signed [31:0] sampled_fir_q_wave;

    always @(posedge aclk)
    begin
        if(aresetn)
        begin
            /* Read a new sample of input signal each clock cycle */
            $fscanf (fi, "%d", test_chirp_discrete);
            /* Dump signals to file */
            sampled_test_signal <= test_chirp_discrete;
            sampled_cmpy_out_i <= dut.fir_tb_bd_i.cmpy_M_AXIS_DOUT_TDATA[31:0];
            sampled_cmpy_out_q <= dut.fir_tb_bd_i.cmpy_M_AXIS_DOUT_TDATA[63:32];
            sampled_cic_out_i <= dut.fir_tb_bd_i.cic_compiler_i_M_AXIS_DATA_TDATA;
            sampled_cic_out_q <= dut.fir_tb_bd_i.cic_compiler_q_M_AXIS_DATA_TDATA;
            sampled_fir_i_wave <= M_AXIS_OUT_tdata[31:0];
            sampled_fir_q_wave <= M_AXIS_OUT_tdata[63:32];
            $fdisplay(fd,"%d,%d,%d,%d,%d,%d,%d",sampled_test_signal,sampled_cmpy_out_i,sampled_cmpy_out_q,sampled_cic_out_i,sampled_cic_out_q,sampled_fir_i_wave,sampled_fir_q_wave);
        end
    end

endmodule