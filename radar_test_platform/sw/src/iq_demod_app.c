#include "common.h"
#include "iq_demod_app.h"
#include "FreeRTOS.h"
#include "lwipopts.h"
#include "netif/xadapter.h"

#include "messages.pb.h"

void iq_demod_app_thread(void *p);

extern void send_ack(xQueueHandle queue, Ack_msg_Retval retval);

/* Protobuf message for demodulator samples */
Debug_msg demod_samples_msg;

int iq_demod_app_init (iq_demod_app_t *app, IQ_Demod_t *demod, Waveform_Generator_t *wg, Base_msg *first_message, xQueueHandle net_in_queue, xQueueHandle main_queue, xQueueHandle net_out_queue){
    
	int retval = 0;

    *app = (iq_demod_app_t){0};

    app->demod = demod;
    app->generator = wg;

    // /* Init demodulator instance */
    // iq_demod_init(demod,MY_DEMODULATOR_ADDRESS,DEMODULATOR_TRANSFER_DMA_ID);

    /* Apply first configuration */
    retval = iq_demod_app_decode_config(app,first_message);

    if (retval == 0){

    	print_info("%s: Applying first config. \r\n",__FUNCTION__);

		/* Init queue handles - Queues are already created in main app*/
		app->net_in_queue = net_in_queue;
		app->main_app_queue = main_queue;
		app->net_out_queue = net_out_queue;

		/* Launch task */
		sys_thread_new("iq_demod_app", iq_demod_app_thread,
			(void*)app,
			THREAD_STACKSIZE,
			DEFAULT_THREAD_PRIO);

    }
    else {
    	print_info("%s: First config WRONG. \r\n",__FUNCTION__);
    }

    return retval;
}

int iq_demod_app_decode_config(iq_demod_app_t *app, Base_msg *config_message){
    
    Demodulator_Config_msg *config;
    int retval = 0;

    rates_e config_rate;
    lo_freq_e lo_freq;

    /* Is the received message a configuration ?
    *  Is the received message a demodulator configuration ?
    *  */
    if (config_message->which_message == Base_msg_config_tag &&
        config_message->config.which_config == Config_msg_demodulator_tag){
        
        /* Read pointer to actual demodulator config */
        config = &(config_message->config.demodulator);

        /* Decode source */
        switch (config->source)
        {
        case Demodulator_Config_msg_Source_INTERNAL:
            iq_demod_set_source_internal(app->demod); 
            app->demod->source = INTERNAL;
            break;
        case Demodulator_Config_msg_Source_EXTERNAL:
            iq_demod_set_source_external(app->demod);
            app->demod->source = EXTERNAL;
            break;
        default:
            retval = -1;
            break;
        }

        /* Decode decimation rate */
        switch (config->rate)
        {
            case Demodulator_Config_msg_Rates_RATE_4:
                config_rate = RATE_4; break;
            case Demodulator_Config_msg_Rates_RATE_5:
                config_rate = RATE_5; break;
            case Demodulator_Config_msg_Rates_RATE_8:
                config_rate = RATE_8; break;
            case Demodulator_Config_msg_Rates_RATE_16:
                config_rate = RATE_16; break;
            case Demodulator_Config_msg_Rates_RATE_40:
                config_rate = RATE_40; break;
            case Demodulator_Config_msg_Rates_RATE_70:
                config_rate = RATE_70; break;
            default:
                retval = -1;
                break;
        }

        /* Apply decimation rate */
        if (retval != -1)
        {
            app->demod->decimation_rate = config_rate;
            retval = iq_demod_set_decimation(app->demod,config_rate);
        }

        /* Decode local oscillator freq */
        switch (config->lo_freq)
        {
            case Demodulator_Config_msg_Lo_Freq_LOCAL_OSC_10M:
                lo_freq = LOCAL_OSC_10M; break;
            case Demodulator_Config_msg_Lo_Freq_LOCAL_OSC_20M:
                lo_freq = LOCAL_OSC_20M; break;
            case Demodulator_Config_msg_Lo_Freq_LOCAL_OSC_30M:
                lo_freq = LOCAL_OSC_30M; break;
            case Demodulator_Config_msg_Lo_Freq_LOCAL_OSC_40M:
                lo_freq = LOCAL_OSC_40M; break;
            default:
            retval = -1;
                break;
        }
        
        if (retval != -1)
        {   
            app->demod->lo_freq_khz = lo_freq;
            iq_demod_set_lo(app->demod,lo_freq);
        }

        /* Decode generator configurator if internal source was selected */
        if (app->demod->source == INTERNAL)
        {
            uint32_t low_freq = (uint32_t)lo_freq - (config->test_bw_khz/2);
            uint32_t high_freq = (uint32_t)lo_freq + (config->test_bw_khz/2);

            /* If generator on continuous mode */
            if (config->test_period_us == 0)
            {
                retval = set_continuous_mode_freq_mod(app->generator,low_freq,high_freq, config->test_pulse_length_us);
            }
            else
            {
                retval = set_pulsed_mode_freq_mod(app->generator,config->test_period_us,config->test_pulse_length_us,low_freq,high_freq);
            }
            
            
        }
    }
    else{
        retval = -1;
    }

    return retval;
}


void iq_demod_app_decode_control(iq_demod_app_t *app, Base_msg *config_message){
    
    Control_msg *control;
    
    int valid_message = 0;
    int transfer_error = 0;
    int transfer_is_valid = 0;

    /* Is the received message a control message? */
    if (config_message->which_message == Base_msg_control_tag){
        control = &(config_message->control);

        switch (control->command)
        {
        case Control_msg_Command_START:
            /* Start demodulator */
            iq_demod_start(app->demod);
            /* Also start generator if source is internal */
            if(app->demod->source == INTERNAL){
                generator_start(app->generator);
            }
            valid_message = 1;
            print_info("%s: START. \r\n",__FUNCTION__);
            break;
        
        case Control_msg_Command_STOP:
            /*Stop demodulator */
            iq_demod_stop(app->demod);
            /* Also stop generator if source is internal */
            if(app->demod->source == INTERNAL){
                generator_stop(app->generator);
            }
            valid_message = 1;
            print_info("%s: STOP. \r\n",__FUNCTION__);
            break;
        
        case Control_msg_Command_TRIG_TRANSFER:
            valid_message = 1;
            /* Trigger samples transfer  */
            /* This gets samples from PL to PS */
            print_info("%s: DMA transfer triggered. \r\n",__FUNCTION__);
            if (iq_demod_trigger_transfer(app->demod, control->num_transfer_samples) < 0 ){
                transfer_error  = 1;
            }
            else{
                /* Successful DMA transfer */
                /* Build protobuf message  */
                iq_demod_get_i_samples(app->demod, demod_samples_msg.i_samples, control->num_transfer_samples);
                iq_demod_get_q_samples(app->demod, demod_samples_msg.q_samples, control->num_transfer_samples);
                demod_samples_msg.num_samples = control->num_transfer_samples;
                transfer_is_valid = 1;
                //TODO: Check errors
            }
            break;
        
        default:
            valid_message = 0;
            break;
        }
    }
    else {
        valid_message = 0;
    }

    /* Check message errors */
        if (!valid_message){
            send_ack(app->net_out_queue, Ack_msg_Retval_BAD_COMMAND);

        }
        else{
            /* DMA debug transfer was successful. Inform that debug samples are valid */
            if(transfer_is_valid){
                send_ack(app->net_out_queue, Ack_msg_Retval_DEMOD_TRANSFER_IS_VALID);
            }
            else if (transfer_error)
            {
                send_ack(app->net_out_queue, Ack_msg_Retval_DEBUG_ERROR);
            }
            else{
                send_ack(app->net_out_queue, Ack_msg_Retval_ACK);
            }
        }

}

void iq_demod_app_thread(void *p){

    iq_demod_app_t *app = (iq_demod_app_t*) p;
    
    /* Received message can be Config or Control
     * so we create a Base_msg container for both */
    Base_msg received_message = Base_msg_init_zero;

    int exit = 0;
    while (!exit){
        /* Block until new incoming message */
        xQueueReceive(app->net_in_queue,(void *) &received_message,portMAX_DELAY);

        /* Parse received message */
        switch (received_message.which_message)
        {
        case Base_msg_config_tag:
            /* Is my config ? */
            if (received_message.config.which_config == Config_msg_demodulator_tag){
                /* Decode and set configuration */
                if (iq_demod_app_decode_config(app, &received_message) < 0){
                    send_ack(app->net_out_queue, Ack_msg_Retval_BAD_CONFIG);
                }
                else {
                	print_info("%s: New config applied. \r\n",__FUNCTION__);
                    send_ack(app->net_out_queue, Ack_msg_Retval_ACK);
                }
                
            }
            else{
                exit = 1;
                /* Received generator config, send message to main_app and exit thread */
				xQueueSend(app->main_app_queue, &received_message, portMAX_DELAY);
            }      
            break;
        
        case Base_msg_control_tag:
        	/* Broken conn?*/
        	if (received_message.control.command == Control_msg_Command_BROKEN_CONN){
                exit = 1;
            }
            else{ /* New control command*/
                iq_demod_app_decode_control(app, &received_message);
            }
            break;

        default:
            print_info("%s: Unknown message received \r\n",__FUNCTION__);
            send_ack(app->net_out_queue, Ack_msg_Retval_INVALID_MSG);
            break;
        }    
    }

    print_info("%s: Exiting task. \r\n",__FUNCTION__);
    iq_demod_stop(app->demod);
    generator_stop(app->generator);
    vTaskDelete(NULL);
}
