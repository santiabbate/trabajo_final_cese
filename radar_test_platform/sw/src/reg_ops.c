#include "reg_ops.h"

/**
 * @brief Reads a specific addres.
 * Wrapper of xilinx function, abstraction to read from generator registers
 * 
 * @param r Register address
 * @return uint32_t Register value
 */
uint32_t _readReg(uint32_t r)
{
    return Xil_In32(r);
}

/**
 * @brief Writes to a specific addres.
 * Wrapper of xilinx function, abstraction to write from generator registers
 * 
 * @param addr Register Addres
 * @param data Data to be written
 */
void _writeReg(uint32_t addr, uint32_t data)
{
    Xil_Out32(addr, data);
}

/**
 * @brief Read a specific bit value form the specified address.
 * 
 * @param addr Register Address
 * @param bit bit number (0 to 31)
 * @return uint32_t 0 if FALSE 1 if TRUE
 */
uint32_t _readBit(uint32_t addr, uint32_t bit)
{
    /* Read entire reg */
    uint32_t temp = _readReg(addr);
    // Enmascaro el bit solicitado
    temp &= 1 << bit;
    // Devuelvo el valor
    return (temp ? 1 : 0);
}
/**
 * @brief 
 * 
 * @param addr Writes a specific bit value form the specified address.
 * @param bit bit number (0 to 31)
 * @param value Bit value (True/False, 1/0)
 */
void _writeBit(uint32_t addr, uint32_t bit, uint32_t value)
{   
    /* Read current register value */
    uint32_t reg = _readReg(addr);

    /* Change actual bit */
    if (value)
    {
        reg = reg | (1 << bit);
    }
    else
    {
        reg = reg & ~(1 << bit);
    }

    /* Rewrite modified value */
    _writeReg(addr,reg);
}