#ifndef __REG_OPS
#define __REG_OPS

#include <xil_io.h>

uint32_t _readReg(uint32_t r);
void _writeReg(uint32_t addr, uint32_t data);
uint32_t _readBit(uint32_t addr, uint32_t bit);
void _writeBit(uint32_t addr, uint32_t bit, uint32_t value);


#endif