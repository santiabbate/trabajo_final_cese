#ifndef __DEMODULATOR_APP
#define __DEMODULATOR_APP

#include "FreeRTOS.h"
#include "queue.h"
#include "iq_demod.h"
#include "generator.h"
#include "messages.pb.h"

/* From xparameters.h */
#define MY_DEMODULATOR_ADDRESS XPAR_MM2IQ_CONFIG_BASEADDR
#define DEMODULATOR_TRANSFER_DMA_ID XPAR_AXI_DMA_IQ_DEMOD_DEVICE_ID


typedef struct {
    IQ_Demod_t *demod;
    Waveform_Generator_t *generator;
    Base_msg incoming_message;

    /* Queue for incoming network messages*/
    xQueueHandle net_in_queue;
    /* Queue for main app communication */
    xQueueHandle main_app_queue;
    /* Network output data queue */
    /* This queue is initialized in main_app*/
    xQueueHandle net_out_queue;
}iq_demod_app_t;


int iq_demod_app_init (iq_demod_app_t *app, IQ_Demod_t *demod, Waveform_Generator_t *wg, Base_msg *first_message, xQueueHandle net_in_queue, xQueueHandle main_queue, xQueueHandle net_out_queue);

int iq_demod_app_decode_config(iq_demod_app_t *app, Base_msg *first_message);

#endif
