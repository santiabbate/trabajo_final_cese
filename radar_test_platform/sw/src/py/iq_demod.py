import socket
import messages_pb2
import enum
import numpy as np
import matplotlib
import matplotlib.pyplot as plt


class InitError(Exception):
    def __init__(self, message):
        self.message = "Init Error: " + message
        super().__init__(self.message)
    pass

class AckError(Exception):
    def __init__(self, message):
        self.message = "Ack Error:" + message
    pass


class IQ_Demod:
    """IQ Demodulator class for RADAR test platform

    Attributes:
        base_msg (protobuf): Base Message container for protobuf encoded messages This message will be sent to the board. See messages.proto file
        config (protobuf): IQ_Demod configuration parameters as protobuf message. See messages.proto file
        control (protobuf): IQ_Demod control commands as protobuf message. See messages.proto file

    """
    # Protobuf objects creation
    base_msg = messages_pb2.Base_msg()
    config = messages_pb2.Demodulator_Config_msg()
    control = messages_pb2.Control_msg()
    
    fs = 125000000
    class Lo_Freq(enum.Enum):
        """Enumeration with legal local oscillator configurations
        """
        LOCAL_OSC_10M = 0
        LOCAL_OSC_20M = 1
        LOCAL_OSC_30M = 2
        LOCAL_OSC_40M = 3

    class Rate(enum.Enum):
        """Enumeration with legal decimation rate configurations
        """
        RATE_4 = 0
        RATE_5 = 1
        RATE_8 = 2
        RATE_16 = 3
        RATE_40 = 4
        RATE_70 = 5

    class Test_BW(enum.Enum):
        """Enumeration with legal bandwith configurations of internally generated test signal 
        """
        BW_1M = 0
        BW_2M = 1
        BW_5M = 2
        BW_10M = 3
        BW_15M = 4
        BW_20M = 5

    def __init__(self, sock=0, net_timeout=5):
        """IQ_Demod init

        Args:
            sock (int, optional): TCP Socket for data transfer to radar platform.
                Defaults to 0 and creates an internal socket if not passed as argument.
            net_timeout (int, optional): Optional network timeout value. Defaults to 5.

        Raises:
            InitError: Couldn't connect to radar platform. Connection Timeout.
        """

        # Create and connect socket if no open socket was received
        if sock == 0:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.settimeout(net_timeout)
            self.server_address = ('192.168.1.10', 7)
            print('Connecting to {} port {}'.format(
                self.server_address[0], self.server_address[1]))
            try:
                self.sock.connect(self.server_address)
                print("Connected")
                self.connected = True
            except Exception as e:
                self.connected = False
                self.sock.close()
                raise InitError("Connect timeout")
        else:
            self.sock = sock

        self.started = False
        

        # Apply intial configuration
        self.set_source_internal(send=False)
        self.set_local_oscillator(self.Lo_Freq.LOCAL_OSC_40M, send=False)
        self.set_decimation(self.Rate.RATE_4, send=False)
        self.set_internal_test_params(self.Test_BW.BW_20M, 100, period_us=0, send=True)

    def start(self):
        """Sends start command through socket.

        Raises:
            AckError: If bad or corrupted command was detected, or No Config was applied.

        Returns:
            bool: True on success.
        """
        # Set command and serialize it
        self.control.command = self.control.START
        serial = self.__serialize_control__()
        # Send through socket
        self.sock.send(serial)
        # Receive and decode response
        input = self.sock.recv(100)
        retmsg = messages_pb2.Base_msg()
        retmsg.ParseFromString(input)
        if retmsg.ack.retval == messages_pb2.Ack_msg.ACK:       # Success!
            self.started = True                                 # IQ Demod started
            return True
        # Raise errors depending on response
        elif retmsg.ack.retval == messages_pb2.Ack_msg.NO_CONFIG:
            raise AckError("No Config")
        elif retmsg.ack.retval == messages_pb2.Ack_msg.BAD_COMMAND:
            raise AckError("Bad Command")

    def stop(self):
        """Sends stop command through socket.

        Raises:
            AckError: If bad or corrupted command was detected.

        Returns:
            bool: True on success.
        """
        # Set command and serialize it
        self.control.command = self.control.STOP
        serial = self.__serialize_control__()
        # Send through socket
        self.sock.send(serial)
        # Receive and decode response
        input = self.sock.recv(100)
        retmsg = messages_pb2.Base_msg()
        retmsg.ParseFromString(input)
        if retmsg.ack.retval == messages_pb2.Ack_msg.ACK:
            self.started = False
            return True
        else:
            raise AckError("Stop Error")

    def trigger_transfer(self, num_samples):
        """Sends command to trigger transfer of demodulated samples.

        Args:
            num_samples (int): Number of samples to transfer.

        Raises:
            AckError: On transfer error.
        """
        # Set command and serialize it
        self.control.command = self.control.TRIG_TRANSFER
        self.control.num_transfer_samples = num_samples
        serial = self.__serialize_control__()
        # Send command
        self.sock.send(serial)
        # Receive and decode response
        fragments = []
        while True:
            try:
                chunk = self.sock.recv(500000)
                fragments.append(chunk)
            except:
                break
        input = b''.join(fragments)
        retmsg = messages_pb2.Debug_msg()
        try:
            # Samples are returned in protobuf message format
            # See "Debug_msg" in messages.proto file
            retmsg.ParseFromString(input)
            self.i_samples = retmsg.i_samples
            self.q_samples = retmsg.q_samples
            self.num_samples = retmsg.num_samples
        except:
            raise AckError("Demod transfer Error")


    def set_source_internal(self, send=True):
        """Sets demodulator source from internally generated debug signal

        Args:
            send (bool, optional): Selects whether to send configuration to the board,
            or to set it locally. Defaults to True.
        """
        self.config.source = self.config.INTERNAL
        if send:
            self.__send_config__()

    def set_source_external(self, send=True):
        """Sets demodulator source from external input signal

        Args:
            send (bool, optional): Selects whether to send configuration to the board,
            or to set it locally. Defaults to True.
        """
        self.config.source = self.config.EXTERNAL
        if send:
            self.__send_config__()

    def set_local_oscillator(self, lo_freq, send=True):
        """Sets local oscillator frequency value.

        Args:
            lo_freq (Lo_Freq enum): Local oscillator frequency preset.
            send (bool, optional): Selects whether to send configuration to the board,
            or to set it locally. Defaults to True.
        """
        # This translates Enumerated values in class, to protobuf message configured values
        if lo_freq == self.Lo_Freq.LOCAL_OSC_10M:
            self.config.lo_freq = self.config.LOCAL_OSC_10M
        elif lo_freq == self.Lo_Freq.LOCAL_OSC_20M:
            self.config.lo_freq = self.config.LOCAL_OSC_20M
        elif lo_freq == self.Lo_Freq.LOCAL_OSC_30M:
            self.config.lo_freq = self.config.LOCAL_OSC_30M
        elif lo_freq == self.Lo_Freq.LOCAL_OSC_40M:
            self.config.lo_freq = self.config.LOCAL_OSC_40M
        if send:
            self.__send_config__()

    def set_decimation(self, rate, send=True):
        """Sets decimation rate for CIC filter in demodulator.

        Args:
            rate (Rate enum): Decimation rate presets.
            send (bool, optional): Selects whether to send configuration to the board,
            or to set it locally. Defaults to True.
        """
        # This translates Enumerated values in class, to protobuf message configured values
        # Also set output rate value depending on decimation rate configured
        if rate == self.Rate.RATE_4:
            self.config.rate = self.config.RATE_4
            self.output_rate = self.fs/4
        elif rate == self.Rate.RATE_5:
            self.config.rate = self.config.RATE_5
            self.output_rate = self.fs/5
        elif rate == self.Rate.RATE_8:
            self.config.rate = self.config.RATE_8
            self.output_rate = self.fs/8
        elif rate == self.Rate.RATE_16:
            self.config.rate = self.config.RATE_16
            self.output_rate = self.fs/16
        elif rate == self.Rate.RATE_40:
            self.config.rate = self.config.RATE_40
            self.output_rate = self.fs/40
        elif rate == self.Rate.RATE_70:
            self.config.rate = self.config.RATE_70
            self.output_rate = self.fs/70
        if send:
            self.__send_config__()

    def __serialize_control__(self):
        """Serialize control message protobuf.

        Returns:
            bytes: Serialized message.
        """
        # Build base_msg from control message, and serialize it
        self.base_msg.control.CopyFrom(self.control)
        self.serial = self.base_msg.SerializeToString()
        return self.serial

    def __serialize_config__(self):
        """Serialize configuration message protobuf.

        Returns:
            bytes: Serialized message
        """
        # Build base_msg from configuration message, and serialize it
        self.base_msg.config.demodulator.CopyFrom(self.config)
        self.serial = self.base_msg.SerializeToString()
        return self.serial

    def __send_config__(self):
        """Sends configuration through socket.

        Raises:
            AckError: If invalid config was detected on the board.
        """
        serial = self.__serialize_config__()
        self.sock.send(serial)
        input = self.sock.recv(100)
        retmsg = messages_pb2.Base_msg()
        retmsg.ParseFromString(input)
        if retmsg.ack.retval == messages_pb2.Ack_msg.BAD_CONFIG:
            raise AckError("Bad Config")

    def set_internal_test_params(self, bw, pulse_length_us, pulsed=False, period_us=250, send=True):
        """Sets internally generated debug signal parameters.
        Debug signals are chirps centered on intermediate frequency.

        Args:
            bw (Test_BW enum): Chirp bandwith presets.
            pulse_length_us (int): Length of chirp in microseconds.
            pulsed (bool, optional): Selects if test signal is pulsed. Defaults to False.
            period_us (int, optional): Period for pulsed mode. Defaults to 250 (max. pssible value).
            send (bool, optional): Selects whether to send configuration to the board,
            or to set it locally. Defaults to True.
        """
        # This translates Enumerated values in class, to protobuf message values
        if bw == self.Test_BW.BW_1M:
            self.config.test_bw_khz = 1000
        elif bw == self.Test_BW.BW_2M:
            self.config.test_bw_khz = 2000
        elif bw == self.Test_BW.BW_5M:
            self.config.test_bw_khz = 5000
        elif bw == self.Test_BW.BW_10M:
            self.config.test_bw_khz = 10000
        elif bw == self.Test_BW.BW_15M:
            self.config.test_bw_khz = 15000
        elif bw == self.Test_BW.BW_20M:
            self.config.test_bw_khz = 20000

        self.config.test_pulse_length_us = pulse_length_us

        if pulsed:
            self.config.test_period_us = period_us
        else:
            self.config.test_period_us = 0

        if send:
            self.__send_config__()

    def plot_specgram(self, samples, file_name = 'fig', print = False):
        if self.num_samples != 0:
            i = np.array(self.i_samples[0:0 + samples-1])
            q = np.array(self.q_samples[0:0 + samples-1])
            self.samples_array = i + 1j*q
            fig, ax = plt.subplots()
            Pxx, freqs, bins, im = ax.specgram(
                self.samples_array, NFFT=32, noverlap=20, Fs=self.output_rate, mode = 'magnitude')
            # ax.set_ylim(-1.5e6,1.5e6)
            cbar = fig.colorbar(im)
            cbar.set_label('Magnitud [dB]')

            plt.xlabel('Tiempo [s]')
            plt.ylabel('Frecuencia [Hz]')
            # plt.title(file_name)

            if print:
                plt.savefig(file_name + '.pdf')
            else:
                plt.show()

def main():
    pass


if __name__ == "__main__":
    # execute only if run as a script
    main()
