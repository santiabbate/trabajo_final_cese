import socket
import messages_pb2
import matplotlib
import matplotlib.pyplot as plt

import numpy as np
import math
from scipy.signal import find_peaks

class InitError(Exception):
    def __init__(self, message):
        self.message = "Init Error:" + message
    pass

class AckError(Exception):
    def __init__(self, message):
        self.message = "Ack Error:" + message
    pass

class Generator:
    """Generator class for RADAR test platform
    """
    base_msg = messages_pb2.Base_msg()
    config = messages_pb2.Generator_Config_msg()
    control = messages_pb2.Base_msg()

    fs = 125000000
    ts = 1/fs

    def __init__(self, sock = 0, net_timeout = 5):
        """Generator init

        Args:
            sock (int, optional): TCP Socket for data transfer to radar platform.
                Defaults to 0 and creates an internal socket if not passed as argument.
            net_timeout (int, optional): Optional network timeout value. Defaults to 5.
        """
        # Create and connect socket if no open socket was received
        if sock == 0:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.settimeout(net_timeout)
            self.server_address = ('192.168.1.10', 7)
            print('Connecting to {} port {}'.format(
                self.server_address[0], self.server_address[1]))
            try:
                self.sock.connect(self.server_address)
                print("Connected")
                self.connected = True
            except Exception as e:
                self.connected = False
                self.sock.close()
                raise InitError("Connect timeout")
        else:
            self.sock = sock

        self.started = False

    def enable_debug(self, val = True):
        self.config.debug_enabled = val

    def __serialize_config__(self):
        """Serialize configuration message protobuf.

        Returns:
            bytes: Serialized message
        """
        self.base_msg.config.generator.CopyFrom(self.config)
        self.serial = self.base_msg.SerializeToString()
        return self.serial

    def __send_config__(self):
        """Sends configuration through socket.

        Raises:
            AckError: If invalid config was detected on the board.
        """
        serial = self.__serialize_config__()
        self.sock.send(serial)
        input = self.sock.recv(100)
        retmsg = messages_pb2.Base_msg()
        retmsg.ParseFromString(input)
        if retmsg.ack.retval == messages_pb2.Ack_msg.BAD_CONFIG:
            raise AckError("Bad Config")

    def set_continuous_mode_constant_freq(self, freq_khz):
        """Sets generator in continuous mode, with a constant frequency value.

        Args:
            freq_khz (int): Frequency value in kilohertz. Max frequency: 50000 kHz.
        """
        # Build protobuf message fields from entered configuration values
        self.config.mode = self.config.CONTINUOUS
        self.config.const_freq.freq_khz = freq_khz
        self.__send_config__()
    
    def set_continuous_mode_freq_mod(self, low_freq_khz, high_freq_khz, length_us):
        """Sets generator in continuous mode, with a linear frequency sweep modulation

        Args:
            low_freq_khz (int): Initial sweep frequency value in kilohertz.
            high_freq_khz (int): Final sweep frequency value in kilohertz. Max frequency: 50000 kHz.
            length_us (int): Time length of the sweep in microseconds. Max puhlse length: 200 us.
        """
        # Build protobuf message fields from entered configuration values
        self.config.mode = self.config.CONTINUOUS
        self.config.freq_mod.low_freq_khz = low_freq_khz
        self.config.freq_mod.high_freq_khz = high_freq_khz
        self.config.freq_mod.length_us = length_us
        self.__send_config__()

    def set_continuous_mode_phase_mod(self, freq_khz, barker_seq_num, barker_subpulse_length_us):
        """Sets generator in continuous mode, with Barker's code phase modulation.

        Args:
            freq_khz (int): Frequency value in kilohertz. Max frequency: 50000 kHz.
            barker_seq_num (int): Barker code sequence number. Possible values are: 2, 3, 4, 5, 7, 11, 13.
            barker_subpulse_length_us (int): Duration in microseconds of each of the N subpulses. Min value: 1 us. Max value: (Duration * barker seq num) < 200 us.
        """
        # Build protobuf message fields from entered configuration values
        self.config.mode = self.config.CONTINUOUS
        self.config.phase_mod.freq_khz = freq_khz
        self.config.phase_mod.barker_seq_num = barker_seq_num
        self.config.phase_mod.barker_subpulse_length_us = barker_subpulse_length_us
        self.__send_config__()

    def set_pulsed_mode_constant_freq(self, period_us, pulse_length_us, freq_khz):
        """Sets generator in pulsed mode, with a constant frequeny value

        Args:
            period_us (int): Period of the signal in microseconds. Max value: 250 us.
            pulse_length_us (int): Pulse length of the signal in microseconds. Max value: 200 us.
            freq_khz (int): Constant frequency value in kiloherthz. Max value: 50000 kHz.
        """
        # Build protobuf message fields from entered configuration values
        self.config.mode = self.config.PULSED
        self.config.period_us = period_us
        self.config.pulse_length_us = pulse_length_us
        self.config.const_freq.freq_khz = freq_khz
        self.__send_config__()

    def set_pulsed_mode_freq_mod(self, period_us, pulse_length_us, low_freq_khz, high_freq_khz):
        """Sets generator in pulsed mode, with a linear frequency sweep modulation.

        Args:
            period_us (int): Period of the signal in microseconds. Max value: 250 us.
            pulse_length_us (int): Pulse length of the signal in microseconds. Max value: 200 us.
            low_freq_khz (int): Initial sweep frequency value in kilohertz.
            high_freq_khz (int): Final sweep frequency value in kilohertz. Max frequency: 50000 kHz.
        """
        # Build protobuf message fields from entered configuration values
        self.config.mode = self.config.PULSED
        self.config.period_us = period_us
        self.config.pulse_length_us = pulse_length_us
        self.config.freq_mod.low_freq_khz = low_freq_khz
        self.config.freq_mod.high_freq_khz = high_freq_khz
        self.__send_config__()

    def set_pulsed_mode_phase_mod(self, period_us, pulse_length_us, freq_khz, barker_seq_num):
        """Sets generator in pulsed mode, with Barker's code phase modulation.

        Args:
            period_us (int): Period of the signal in microseconds. Max value: 250 us.
            pulse_length_us (int): Pulse length of the signal in microseconds. Max value: 200 us.
            freq_khz (int): Frequency value in kilohertz. Max frequency: 50000 kHz.
            barker_seq_num (int): Barker code sequence number. Possible values are: 2, 3, 4, 5, 7, 11, 13.
        """
        # Build protobuf message fields from entered configuration values
        self.config.mode = self.config.PULSED
        self.config.period_us = period_us
        self.config.pulse_length_us = pulse_length_us
        self.config.phase_mod.freq_khz = freq_khz
        self.config.phase_mod.barker_seq_num = barker_seq_num
        self.__send_config__()

    def start(self):
        """Sends start command through socket.

        Raises:
            AckError: If bad or corrupted command was detected, or No Config was applied.

        Returns:
            bool: True on success.
        """
        # Set command and serialize it
        self.control.control.command = self.control.control.START
        serial = self.control.SerializeToString()
        # Send through socket
        self.sock.send(serial)
        # Receive and decode response
        input = self.sock.recv(100)
        retmsg = messages_pb2.Base_msg()
        retmsg.ParseFromString(input)
        if retmsg.ack.retval == messages_pb2.Ack_msg.ACK:           # Success!
            return True
        # Raise errors depending on response
        elif retmsg.ack.retval == messages_pb2.Ack_msg.NO_CONFIG:
            raise AckError("No Config")
        elif retmsg.ack.retval == messages_pb2.Ack_msg.BAD_COMMAND:
            raise AckError("Bad Command")
        
    def stop(self):
        """Sends stop command through socket.

        Raises:
            AckError: AckError: If bad or corrupted command was detected.

        Returns:
            bool: True on success.
        """
        # Set command and serialize it
        self.control.control.command = self.control.control.STOP
        serial = self.control.SerializeToString()
        # Send through socket
        self.sock.send(serial)
        # Receive and decode response
        input = self.sock.recv(100)
        retmsg = messages_pb2.Base_msg()
        retmsg.ParseFromString(input)
        if retmsg.ack.retval == messages_pb2.Ack_msg.ACK:
            return True
        else:
            raise AckError("Stop Error")

    def trigger_debug(self):
        """Sends command to trigger transfer o demodulated samples.

        Raises:
            AckError:  On transfer error.
        """
        self.control.control.command = self.control.control.TRIG_DBG
        serial = self.control.SerializeToString()
        self.sock.send(serial)
        # self.sock.settimeout(0)
        fragments = []
        while True:
            try: 
                chunk = self.sock.recv(500000)
                fragments.append(chunk)
            except: 
                break
        input = b''.join(fragments)
        retmsg = messages_pb2.Debug_msg()
        try:
            retmsg.ParseFromString(input)
            self.i_samples = retmsg.i_samples
            self.q_samples = retmsg.q_samples
            self.num_samples = retmsg.num_samples
        except:
            raise AckError("Debug Error")

    def dump_samples(self):
        with open("dump.txt","w+") as f: 
            for i in range(self.num_samples):
                f.write("%s,%s\n" % (self.i_samples[i], self.q_samples[i]))
            f.close()

    def plot_samples(self, new_samples = True, time_us = None, print = False, file_name = 'fig'):
        if new_samples:
            self.start()
            self.trigger_debug()

        if self.num_samples != 0:
            start = 100
            t_start = start * self.ts
            fig, ax = plt.subplots(2,1, sharex=True, sharey=True)
            if time_us == None:
                samples = self.num_samples
            else:
                samples = round((time_us / 1000000) / self.ts)

            t_stop = samples * self.ts
            time = np.arange(0,t_stop,self.ts)
            samples = len(time)
            ax[0].plot(time, self.i_samples[start:start + samples])
            ax[0].set_title('Muestras Generadas I')

            ax[1].plot(time, self.q_samples[start:start + samples])
            ax[1].set_title('Muestras Generadas Q')
            plt.yticks([])
            
            ax[0].set_xlabel('Tiempo [s]')
            ax[1].set_xlabel('Tiempo [s]')

            ax[0].set_ylabel('Amplitud')
            ax[1].set_ylabel('Amplitud')

            fig.tight_layout()
            # plt.grid(True)
            if print:
                plt.savefig(file_name + '.pdf')
            else:
                plt.show()

    def plot_spectrum(self, new_samples = True, print = False, file_name = 'fig', xlim_khz = None):
        if new_samples:
            self.start()
            self.trigger_debug()

        if self.num_samples != 0:
            i = np.array(self.i_samples)
            q = np.array(self.q_samples)
            self.samples_array = i + 1j*q
            fig, ax = plt.subplots()

            ax.magnitude_spectrum(self.samples_array, Fs=self.fs, scale='dB')
            ax.set_xlabel('Frecuencia [Hz]')
            ax.set_ylabel('Magnitud [dB]')
            ax.set_title('Espectro señal de salida')

            if (xlim_khz != None):
                ax.set_xlim(xlim_khz[0]*1e3,xlim_khz[1]*1e3)

            if print:
                plt.savefig(file_name + '.pdf')
            else:
                plt.show()

    def plot_xcorr(self, pulse_length_us, freq_khz, barker_seq_num, time_us = None, new_samples = True, print = False, file_name = 'fig'):
        """Compute and plot cross-correlation between samples downloaded from Generator and locally generated signal.

        Args:
            pulse_length_us (int): Length of ocmplete barker sequence in microseconds.
            new_samples (bool, optional): Set to False if you want to compute correlation with previous downloaded samples. Defaults to True.
            time_us ([type], optional): [description]. Defaults to None.
            print (bool, optional): [description]. Defaults to False.
            file_name (str, optional): [description]. Defaults to 'fig'.
        """

        if new_samples:
            self.start()
            self.trigger_debug()

        if self.num_samples != 0:
            barker_codes = {
                2: [-1,1],
                3: [-1,1,1],
                4: [1,1,-1,1],
                5: [1,-1,1,1,1],
                7: [-1,1,-1,-1,1,1,1],
                11: [-1,1,-1,-1,1,-1,-1,-1,1,1,1],
                13: [1,-1,1,-1,1,1,-1,-1,1,1,1,1,1]
            }

            f = freq_khz*1e3
            length = pulse_length_us * 1e-6    

            # Build sinewave
            t = np.arange(0,length + self.ts, self.ts)
            sine = np.sin(2*np.pi*f*t)

            # Build barker code
            barker_coeff = barker_codes[barker_seq_num]
            subpulse_samples = math.floor((len(t)/barker_seq_num))
            barker = np.ones(len(t))

            start = 0
            end = subpulse_samples + 1

            for i in barker_coeff:
                barker[start:end] = barker[start:end] * i
                start = end
                end = start + subpulse_samples + 1

            # Multiply elementwise
            barker_signal = sine * barker
            if time_us == None:
                samples = self.num_samples
            else:
                samples = round((time_us / 1000000) / self.ts)
            
            i = np.array(self.q_samples[0:samples])
            t_signal = np.array(range(samples)) * self.ts

            # Correlate signals
            corr = np.correlate(i, barker_signal, "full")
            corr = corr / max(corr)
            t_corr = np.array(range(len(corr))) * self.ts
            # Find peaks in correlation 
            peaks,_ = find_peaks(corr, distance = len(t)*1.5)

            fig, ax = plt.subplots(3,1)
            ax[2].plot(t_corr, corr, linewidth = 1)
            ax[2].plot(t_corr[peaks],corr[peaks], "x")

            for p in peaks:
                ax[2].text(t_corr[p] + 0.00003,corr[p] - 2000,'t = ' + str(t_corr[p] * 1e6) + 'us')

            ax[1].plot(t_signal,i, linewidth = 1)
            ax[0].plot(t,barker_signal, linewidth = 1)
            
            ax[2].set_xlabel('Tiempo [s]')
            
            ax[2].yaxis.set_major_locator(plt.NullLocator())
            ax[1].yaxis.set_major_locator(plt.NullLocator())
            ax[0].yaxis.set_major_locator(plt.NullLocator())

            ax[0].set_title('Señal Barker generada en la PC')
            ax[1].set_title('Muestras (Q) generadas en la FPGA')
            ax[2].set_title('Correlación entre ambas')

            # plt.yticks([])

            fig.tight_layout()
            if print:
                plt.savefig(file_name + '.pdf')
            else:
                plt.show()

    def plot_specgram(self, time_us = None,  new_samples = True, print = False, file_name = 'fig', ylim_khz = None):

        if new_samples:
            self.start()
            self.trigger_debug()
        
        if time_us == None:
            samples = self.num_samples
        else:
            samples = round((time_us / 1000000) / self.ts)

        if self.num_samples != 0:
            i = np.array(self.i_samples[0:samples-1])
            q = np.array(self.q_samples[0:samples-1])
            self.samples_array = i + 1j*q
            fig, ax = plt.subplots()
            Pxx, freqs, bins, im = ax.specgram(
                self.samples_array, NFFT=512, noverlap=30, Fs=self.fs, mode = 'magnitude')
            
            cbar = fig.colorbar(im)
            cbar.set_label('Magnitud [dB]')

            plt.xlabel('Tiempo [s]')
            plt.ylabel('Frecuencia [Hz]')
            # plt.title(file_name)

            if (ylim_khz != None):
                ax.set_ylim(ylim_khz[0]*1e3,ylim_khz[1]*1e3)

            fig.tight_layout()
            if print:
                plt.savefig(file_name + '.pdf')
            else:
                plt.show()


def main():
    pass

if __name__ == "__main__":
    # execute only if run as a script
    main()