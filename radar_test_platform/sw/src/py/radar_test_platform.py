import socket
import messages_pb2
from generator import Generator
from iq_demod import IQ_Demod

class InitError(Exception):
    def __init__(self, message):
        self.message = "Init Error: " + message
        super().__init__(self.message)
    pass

class AckError(Exception):
    def __init__(self, message):
        self.message = "Ack Error:" + message
    pass

class Radar_Test_Platform:
    def __init__(self,ip,port, net_timeout = 5, initial_config = 'generator'):
        self.ip = ip
        self.port = port
        
        self.base_msg = messages_pb2.Base_msg()
        self.generator_config = messages_pb2.Generator_Config_msg()
        self.demod_config = messages_pb2.Demodulator_Config_msg()
        self.control = messages_pb2.Control_msg()
 
        self.actual_config = messages_pb2.Config_msg()

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.settimeout(net_timeout)
        self.server_address = (ip, port)
        print('Connecting to {} port {}'.format(self.server_address[0],self.server_address[1]))
        try:
            self.sock.connect(self.server_address)
            print("Connected")
            self.connected = True
        except Exception as e:
            self.connected = False
            self.sock.close()
            raise InitError("Connect timeout")
        
        # Generator object
        self.generator = Generator(sock = self.sock)
        # Demodulator object
        self.demod = IQ_Demod(sock = self.sock)
        # Apply initial config
        # self.demod.set_source_internal()
        pass

    def generator_tests(self):
        gen = self.generator
        try:
            ### Uncomment tests    

            ### Continuous mode constant freq tests
            ### 1) 1MHz Signal
            # gen.set_continuous_mode_constant_freq(1000)
            # gen.plot_samples(time_us = 5, print = True, file_name = '1MHz_Constant')
            # gen.plot_spectrum(new_samples = False, xlim_khz = [-1200,1200], print = True, file_name = '1MHz_Constant_Spectrum')

            ### 2) 20MHz Signal
            # gen.set_continuous_mode_constant_freq(20000)
            # gen.plot_samples(time_us = 1, print = True, file_name = '20MHz_Constant')
            # gen.plot_spectrum(new_samples = False, xlim_khz = [-21000,21000], print = True, file_name = '20MHz_Constant_Spectrum')

            ### Pulsed mode constant freq tests
            ### 1) 200kHz Signal, Pulsed mode, Period 100us, Pulse width 20us
            # gen.set_pulsed_mode_constant_freq(100,20,200)
            # gen.plot_samples(time_us = 280, print = True, file_name = '200kHz_Pulsed_100ms_20ms')

            ### Pulsed mode constant freq tests
            ### 2) 1MHz Signal, Pulsed mode, Period 5us, Pulse width 1us
            # gen.set_pulsed_mode_constant_freq(15,10,1000)
            # gen.plot_samples(time_us = 35, print = True, file_name = '1MHz_Pulsed_15us_10us')

            ### Continuous mode freq modulation tests
            ### 1) DC to 300kHz 
            # gen.set_continuous_mode_freq_mod(0, 300, 250)
            # gen.plot_samples(time_us = 600, print = True, file_name = 'Freq_Mod_Constant_DC_300kHz')
            
            ### 2) 1MHz to 5MHz 100us period
            # gen.set_continuous_mode_freq_mod(1000, 5000, 100)
            # gen.plot_specgram(time_us = 300, ylim_khz = [-8000,8000], print = True, file_name = 'Freq_Mod_Continuous_1MHz_5MHz_Specgram')
            # gen.plot_samples(time_us = 600, print = True, file_name = 'Freq_Mod_Constant_DC_300kHz')

            ### 2) 39.5MHz to 40.5MHz 250us period
            # gen.set_continuous_mode_freq_mod(39500, 40500, 250)
            # gen.plot_specgram(time_us = 1500, ylim_khz = [0,50000], print = True, file_name = 'Freq_Mod_Continuous_39_5MHz_40_5MHz_Specgram')

            ### 3) 5MHz Bandwith at 20MHz LO, 100us period
            # gen.set_continuous_mode_freq_mod(20000-2500, 20000+2500, 100)
            # gen.plot_specgram(time_us = 600, ylim_khz = [0,25000], print = True, file_name = 'Freq_Mod_Continuous_17_5MHz_22_5MHz_Specgram')

            ### 4) 20MHz Bandwith at 30MHz LO, 100us period
            # gen.set_continuous_mode_freq_mod(30000-10000, 30000+10000, 100)
            # gen.plot_specgram(time_us = 600, ylim_khz = [0,50000], print = True, file_name = 'Freq_Mod_Continuous_20_MHz_40_MHz_Specgram')

            ### Pulsed mode freq modulation tests
            ### 1) DC to 20MHz
            # gen.set_pulsed_mode_freq_mod(250,200,0,20000)
            # gen.plot_samples(time_us = 800, print = False, file_name = 'Freq_Mod_Constant_DC_300kHz')
            # gen.plot_specgram(time_us = 800, ylim_khz = [-1000,21000], print = True, file_name = 'Freq_Mod_Pulsed_DC_20MHz_Specgram')

            ### 2) 15MHz Bandwith at 40MHz LO
            # gen.set_pulsed_mode_freq_mod(250,100,40000-7500,40000+7500)
            # gen.plot_samples(time_us = 800, print = False, file_name = 'Freq_Mod_Constant_DC_300kHz')
            # gen.plot_specgram(time_us = 800, ylim_khz = [0,50000], print = True, file_name = 'Freq_Mod_Pulsed_32_5MHz_47_5MHz_Specgram')

            ### 3) 1MHz Bandwith at 10MHz LO
            # gen.set_pulsed_mode_freq_mod(250,100,10000-500,10000+500)
            # gen.plot_samples(time_us = 800, print = False, file_name = 'Freq_Mod_Constant_DC_300kHz')
            # gen.plot_specgram(time_us = 800, ylim_khz = [0,11000], print = True, file_name = 'Freq_Mod_Pulsed_9_5MHz_10_5MHz_Specgram')

            # radar.generator.set_pulsed_mode_freq_mod(250,50,0,5000)
            # plot(radar.generator)

            ### Phase modulation tests
            ### 1) Continuous wave. 1MHz Wave, Barker Code 2, 1us per subpulse
            # gen.set_continuous_mode_phase_mod(1000,2,1)
            # gen.plot_samples(time_us = 15, print = True, file_name = 'Phase_Mod_Cont_Barker_2_1MHz_1us_subpulse')
            # ### 2) Continuous wave. 1MHz Wave, Barker Code 3, 1us per subpulse
            # gen.set_continuous_mode_phase_mod(1000,3,1)
            # gen.plot_samples(time_us = 15, print = True, file_name = 'Phase_Mod_Cont_Barker_3_1MHz_1us_subpulse')
            # ### 3) Continuous wave. 1MHz Wave, Barker Code 4, 1us per subpulse
            # gen.set_continuous_mode_phase_mod(1000,4,1)
            # gen.plot_samples(time_us = 15, print = True, file_name = 'Phase_Mod_Cont_Barker_4_1MHz_1us_subpulse')
            # ### 4) Continuous wave. 1MHz Wave, Barker Code 5, 1us per subpulse
            # gen.set_continuous_mode_phase_mod(1000,5,1)
            # gen.plot_samples(time_us = 15, print = True, file_name = 'Phase_Mod_Cont_Barker_5_1MHz_1us_subpulse')
            # ### 7) Continuous wave. 1MHz Wave, Barker Code 7, 1us per subpulse
            # gen.set_continuous_mode_phase_mod(1000,7,1)
            # gen.plot_samples(time_us = 15, print = True, file_name = 'Phase_Mod_Cont_Barker_7_1MHz_1us_subpulse')
            # ### 8) Continuous wave. 1MHz Wave, Barker Code 11, 1us per subpulse
            # gen.set_continuous_mode_phase_mod(1000,11,1)
            # gen.plot_samples(time_us = 15, print = True, file_name = 'Phase_Mod_Cont_Barker_11_1MHz_1us_subpulse')
            # ### 9) Continuous wave. 1MHz Wave, Barker Code 13, 1us per subpulse
            # gen.set_continuous_mode_phase_mod(1000,13,1)
            # gen.plot_samples(time_us = 20, print = True, file_name = 'Phase_Mod_Cont_Barker_13_1MHz_1us_subpulse')

            # ### 10) Continuous wave correlation. 1MHz Wave, Barker Code 13, 1us per subpulse
            # gen.set_continuous_mode_phase_mod(1000,13,1)
            # gen.plot_xcorr(13,1000,13, time_us = 100, print = True, file_name = 'Phase_Mod_Cont_Barker_13_1MHz_1us_subpulse_Correlation')

            # ### 11) Continuous wave correlation. 1.5MHz Wave, Barker Code 5, 2us per subpulse
            # gen.set_continuous_mode_phase_mod(1500,5,2)
            # gen.plot_xcorr(10,1500,5, time_us = 100, print = True, file_name = 'Phase_Mod_Cont_Barker_5_1.5MHz_2us_subpulse_Correlation')

            ### Pulsed mode Barker correlation
            ### 1) Barker 7
            # gen.set_pulsed_mode_phase_mod(250,70,100,7)
            # gen.plot_xcorr(70,100,7, time_us = 700, print = True, file_name = 'Phase_Mod_Pulsed_100kHz_Correlation')

            ### 2) Barker 13
            # gen.set_pulsed_mode_phase_mod(100,13,1000,13)
            # gen.plot_xcorr(13,1000,13, time_us = 250, print = True, file_name = 'Phase_Mod_Pulsed_1MHz_Correlation')

            pass

        except Exception as e:
                print(e)

    def launch_demod_test(self, lo, bw, pulse_length, rate, spectrogram_periods, pulsed = False, period_us = 250, print = False):
        
        # Build file name from configuration
        # Example: LO_40M_BW_1M_RATE_70.pdf
        file_name = ''
        if lo == self.demod.Lo_Freq.LOCAL_OSC_10M:
            file_name += 'LO_10M'
        elif lo == self.demod.Lo_Freq.LOCAL_OSC_20M:
            file_name += 'LO_20M'
        elif lo == self.demod.Lo_Freq.LOCAL_OSC_30M:
            file_name += 'LO_30M'
        elif lo == self.demod.Lo_Freq.LOCAL_OSC_40M:
            file_name += 'LO_40M'

        if bw == self.demod.Test_BW.BW_1M:
            file_name += '_BW_1M'
        elif bw == self.demod.Test_BW.BW_2M:
            file_name += '_BW_2M'
        elif bw == self.demod.Test_BW.BW_5M:
            file_name += '_BW_5M'
        elif bw == self.demod.Test_BW.BW_10M:
            file_name += '_BW_10M'
        elif bw == self.demod.Test_BW.BW_15M:
            file_name += '_BW_15M'
        elif bw == self.demod.Test_BW.BW_20M:
            file_name += '_BW_20M'

        if rate == self.demod.Rate.RATE_4:
            file_name += '_RATE_4'
        elif rate == self.demod.Rate.RATE_5:
            file_name += '_RATE_5'
        elif rate == self.demod.Rate.RATE_8:
            file_name += '_RATE_8'
        elif rate == self.demod.Rate.RATE_16:
            file_name += '_RATE_16'
        elif rate == self.demod.Rate.RATE_40:
            file_name += '_RATE_40'
        elif rate == self.demod.Rate.RATE_70:
            file_name += '_RATE_70'

        file_name += '_PL_' + str(pulse_length) + 'us'

        self.demod.set_local_oscillator(lo)
        self.demod.set_source_internal()
        self.demod.set_internal_test_params(bw,pulse_length, pulsed = pulsed, period_us = period_us)
        self.demod.set_decimation(rate)
        self.demod.start()
        num_samples = int((spectrogram_periods * pulse_length * 0.000001)*self.demod.output_rate)
        self.demod.trigger_transfer(num_samples) #TODO: Error If not started
        self.demod.stop()
        self.demod.plot_specgram(num_samples, file_name = file_name, print = print)

    def demodulator_tests(self):
        demod = self.demod

        # Test initial configuration
        demod.start()
        demod.trigger_transfer(10000) #TODO: Error If not started
        demod.stop() 
        # demod.plot_specgram(10000)

        ### Uncomment tests

        ### Tests with local oscillator at 40 MHz
        ### Chirp (pulse) length at 250 us
        # lo = demod.Lo_Freq.LOCAL_OSC_40M
        # pulse_length = 250
        
        ### Test signal BW: 1MHz Decimation Rate: 70
        # self.launch_demod_test(lo, demod.Test_BW.BW_1M, pulse_length, demod.Rate.RATE_70, 5, print = True)
        # ### Test signal BW: 2MHz Decimation Rate: 40
        # self.launch_demod_test(lo, demod.Test_BW.BW_2M, pulse_length, demod.Rate.RATE_40, 5, print = True)
        # ### Test signal BW: 5MHz Decimation Rate: 16
        # self.launch_demod_test(lo, demod.Test_BW.BW_5M, pulse_length, demod.Rate.RATE_16, 5, print = True)
        ### Test signal BW: 10MHz Decimation Rate: 8
        # self.launch_demod_test(lo, demod.Test_BW.BW_10M, pulse_length, demod.Rate.RATE_8, 5, print = True)
        # ### Test signal BW: 15MHz Decimation Rate: 5
        # self.launch_demod_test(lo, demod.Test_BW.BW_15M, pulse_length, demod.Rate.RATE_5, 5, print = True)
        # ### Test signal BW: 20MHz Decimation Rate: 4
        # self.launch_demod_test(lo, demod.Test_BW.BW_20M, pulse_length, demod.Rate.RATE_4, 5, print = True)

        ### Tests with local oscillator at 30 MHz
        ### Chirp (pulse) length at 100 us
        # lo = demod.Lo_Freq.LOCAL_OSC_30M
        # pulse_length = 100
        
        # ### Test signal BW: 1MHz Decimation Rate: 70
        # self.launch_demod_test(lo, demod.Test_BW.BW_1M, pulse_length, demod.Rate.RATE_70, 5, print = True)
        # ### Test signal BW: 2MHz Decimation Rate: 40
        # self.launch_demod_test(lo, demod.Test_BW.BW_2M, pulse_length, demod.Rate.RATE_40, 5, print = True)
        # ### Test signal BW: 5MHz Decimation Rate: 16
        # self.launch_demod_test(lo, demod.Test_BW.BW_5M, pulse_length, demod.Rate.RATE_16, 5, print = True)
        # ### Test signal BW: 10MHz Decimation Rate: 8
        # self.launch_demod_test(lo, demod.Test_BW.BW_10M, pulse_length, demod.Rate.RATE_8, 5, print = True)
        # ### Test signal BW: 15MHz Decimation Rate: 5
        # self.launch_demod_test(lo, demod.Test_BW.BW_15M, pulse_length, demod.Rate.RATE_5, 5, print = True)
        # ### Test signal BW: 20MHz Decimation Rate: 4
        # self.launch_demod_test(lo, demod.Test_BW.BW_20M, pulse_length, demod.Rate.RATE_4, 5, print = True)

        ### Tests with local oscillator at 20 MHz
        ### Chirp (pulse) length at 100 us
        # lo = demod.Lo_Freq.LOCAL_OSC_30M
        # pulse_length = 100
        
        ### Test signal BW: 1MHz Decimation Rate: 70
        # self.launch_demod_test(lo, demod.Test_BW.BW_1M, pulse_length, demod.Rate.RATE_70, 5, print = True)
        # ### Test signal BW: 2MHz Decimation Rate: 40
        # self.launch_demod_test(lo, demod.Test_BW.BW_2M, pulse_length, demod.Rate.RATE_40, 5, print = True)
        # ### Test signal BW: 5MHz Decimation Rate: 16
        # self.launch_demod_test(lo, demod.Test_BW.BW_5M, pulse_length, demod.Rate.RATE_16, 5, print = True)
        # ### Test signal BW: 10MHz Decimation Rate: 8
        # self.launch_demod_test(lo, demod.Test_BW.BW_10M, pulse_length, demod.Rate.RATE_8, 5, print = True)
        # ### Test signal BW: 15MHz Decimation Rate: 5
        # self.launch_demod_test(lo, demod.Test_BW.BW_15M, pulse_length, demod.Rate.RATE_5, 5, print = True)
        ### Test signal BW: 20MHz Decimation Rate: 4
        # self.launch_demod_test(lo, demod.Test_BW.BW_20M, pulse_length, demod.Rate.RATE_4, 5, print = True)

        ### Tests with local oscillator at 10 MHz
        ### Chirp (pulse) length at 100 us
        # lo = demod.Lo_Freq.LOCAL_OSC_10M
        # pulse_length = 100
        # period = 250
        
        ### Test signal BW: 1MHz Decimation Rate: 70
        # self.launch_demod_test(lo, demod.Test_BW.BW_1M, pulse_length, demod.Rate.RATE_70, 10, pulsed = True, period_us = period, print = True)
        # ### Test signal BW: 2MHz Decimation Rate: 40
        # self.launch_demod_test(lo, demod.Test_BW.BW_2M, pulse_length, demod.Rate.RATE_40, 5)
        ### Test signal BW: 5MHz Decimation Rate: 16
        # self.launch_demod_test(lo, demod.Test_BW.BW_5M, pulse_length, demod.Rate.RATE_16, 5)
        ### Test signal BW: 10MHz Decimation Rate: 8
        # self.launch_demod_test(lo, demod.Test_BW.BW_15M, pulse_length, demod.Rate.RATE_8, 10, pulsed = True, period_us = period, print = True)
        ### Test signal BW: 15MHz Decimation Rate: 5
        # self.launch_demod_test(lo, demod.Test_BW.BW_15M, pulse_length, demod.Rate.RATE_5, 5)
        # ### Test signal BW: 20MHz Decimation Rate: 4
        # self.launch_demod_test(lo, demod.Test_BW.BW_20M, pulse_length, demod.Rate.RATE_4, 5)



def main():
    try:
        radar = Radar_Test_Platform('192.168.1.10',7)
    except Exception as e:
        print(e)

    radar.demodulator_tests()

    radar.generator_tests()
    
    
if __name__ == "__main__":
    # Execute only if run as a script
    # Run internal tests
    main()