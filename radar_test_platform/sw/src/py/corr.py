import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import math


# parameter BARKER_2 = 2'b10;
# parameter BARKER_3 = 3'b110;
# parameter BARKER_4 = 4'b1011;
# parameter BARKER_5 = 5'b11101;
# parameter BARKER_7 = 7'b1110010;
# parameter BARKER_11 = 11'b11100010010;
# parameter BARKER_13 = 13'b1111100110101;

pulse_length_us = 130
freq_khz = 1
barker_seq_num = 13

barker_codes = {
        2: [1,-1],
        3: [1,1,-1],
        4: [1,-1,1,1],
        5: [1,1,1,-1,1],
        7: [1,1,1,-1,-1,1,-1],
        11: [1,1,1,-1,-1,-1,1,-1,-1,1,-1],
        13: [1,1,1,1,1,-1,-1,1,1,-1,1,-1,1]
    }

f = freq_khz*10e3

fs = 125000000
ts = 1/fs
length = pulse_length_us * 10e-6

# Build sinewave
t = np.arange(0,length,ts)
sine = np.sin(2*np.pi*f*t)

# Build barker code
barker_coeff = barker_codes[barker_seq_num]
subpulse_samples = math.floor((len(t)/barker_seq_num))
barker = np.ones(len(t))

start = 0
end = subpulse_samples + 1

for i in barker_coeff:
    barker[start:end] = barker[start:end] * i
    start = end
    end = start + subpulse_samples + 1

# Multiply elementwise
out = sine * barker

z = np.zeros(100000)

test = np.concatenate((z[0:1000],out,z))

corr = np.correlate(out, test, "full")

fig, ax = plt.subplots()
ax.plot(corr)
plt.show()

print(t)