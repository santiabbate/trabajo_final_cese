connect -url tcp:127.0.0.1:3121
targets -set -nocase -filter {name =~"APU*"}
rst -system
after 3000
targets -set -filter {jtag_cable_name =~ "Digilent Arty Z7 003017A4C943A" && level==0} -index 1
fpga -file /mnt/Archivos/cese/trabajo_final/radar_test_platform/sw/radar_test_platform/_ide/bitstream/radar_test_platform_wrapper_3.bit
targets -set -nocase -filter {name =~"APU*"}
loadhw -hw /mnt/Archivos/cese/trabajo_final/radar_test_platform/sw/radar_test_platform_wrapper/export/radar_test_platform_wrapper/hw/radar_test_platform_wrapper_3.xsa -mem-ranges [list {0x40000000 0xbfffffff}]
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*"}
source /mnt/Archivos/cese/trabajo_final/radar_test_platform/sw/radar_test_platform/_ide/psinit/ps7_init.tcl
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "*A9*#0"}
dow /mnt/Archivos/cese/trabajo_final/radar_test_platform/sw/radar_test_platform/Debug/radar_test_platform.elf
configparams force-mem-access 0
bpadd -addr &main
