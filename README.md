# Carrera de Especialización en Sistemas Embebidos
## Desarrollo de etapa digital para radar pulsado multipropósito

### Santiago Abbate

Repositorio de HDL y software

```
generator --> Módulo generador
iq_demod  --> Módulo demodulador
radar_test_platform --> Plataforma de pruebas del sistema completo
```

Carpetas internas
```
    /hdl    Archivos hdl fuente
    /sw     Archivos fuente del software
```


